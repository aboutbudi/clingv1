<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo 'Cling SkinCare Admin | '.PAGE_TITLE;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- fullCalendar -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/fullcalendar/dist/fullcalendar.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/datatable-FixedHeader/css/fixedHeader.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/datatable-responsive/css/responsive.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/select2/dist/css/select2.min.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <style>
    .example-modal .modal {
      position: relative;
      top: auto;
      bottom: auto;
      right: auto;
      left: auto;
      display: block;
      z-index: 1;
    }

    .example-modal .modal {
      background: transparent !important;
    }
  </style>
</head>
<body class="hold-transition skin-purple sidebar-mini">
<div class="wrapper">
    <?php
        include 'header.php';
        include 'sidebar.php';
        include $content_page;
        include 'footer.php';
    ?>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo URL_USER ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo URL_USER ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo URL_USER ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo URL_USER ?>bower_components/raphael/raphael.min.js"></script>
<script src="<?php echo URL_USER ?>bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?php echo URL_USER ?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="<?php echo URL_USER ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo URL_USER ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo URL_USER ?>bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- InputMask -->
<script src=".<?php echo URL_USER ?>plugins/input-mask/jquery.inputmask.js"></script>
<script src=".<?php echo URL_USER ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src=".<?php echo URL_USER ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- daterangepicker -->
<script src="<?php echo URL_USER ?>bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo URL_USER ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?php echo URL_USER ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Select2 -->
<script src="<?php echo URL_USER ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo URL_USER ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- DataTables -->
<script src="<?php echo URL_USER ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo URL_USER ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script src="<?php echo URL_USER ?>bower_components/datatable-FixedHeader/js/dataTables.fixedHeader.min.js"></script>
<script src="<?php echo URL_USER ?>bower_components/datatable-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo URL_USER ?>bower_components/datatable-responsive/js/responsive.bootstrap.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo URL_USER ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo URL_USER ?>bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo URL_USER ?>js/adminlte.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?php echo URL_USER ?>js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo URL_USER ?>js/demo.js"></script>
<!-- fullCalendar -->
<script src="<?php echo URL_USER ?>bower_components/moment/moment.js"></script>
<script src="<?php echo URL_USER ?>bower_components/fullcalendar/dist/fullcalendar.js"></script>


<!-- page script -->

<script>
  $(function () {

    /* initialize the external events
     -----------------------------------------------------------------*/
    function init_events(ele) {
      ele.each(function () {

        // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
        // it doesn't need to have a start or end
        var eventObject = {
          title: $.trim($(this).text()) // use the element's text as the event title
        }

        // store the Event Object in the DOM element so we can get to it later
        $(this).data('eventObject', eventObject)

        // make the event draggable using jQuery UI
        $(this).draggable({
          zIndex        : 1070,
          revert        : true, // will cause the event to go back to its
          revertDuration: 0  //  original position after the drag
        })

      })
    }

    init_events($('#external-events div.external-event'))

    /* initialize the calendar
     -----------------------------------------------------------------*/
    //Date for the calendar events (dummy data)
    var date = new Date()
    var d    = date.getDate(),
        m    = date.getMonth(),
        y    = date.getFullYear()
    $('#kalender').fullCalendar({
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay'
      },
      buttonText: {
        today: 'today',
        month: 'month',
        week : 'week',
        day  : 'day'
      },
      displayEventTime: false,
      //Random default events
      events    : [
        <?php 
            if (is_array($pesanan_sudah_ditanggapi) || is_object($pesanan_sudah_ditanggapi)){
            foreach($pesanan_sudah_ditanggapi as $pesanan_sudah_ditanggapi)
            {
        ?>
          {
            title          : '<?php echo $pesanan_sudah_ditanggapi['kd_pesanan']; ?>',
            start          : new Date(<?php echo $pesanan_sudah_ditanggapi['tahun']; ?>,date.getMonth(<?php echo $pesanan_sudah_ditanggapi['bulan']; ?>), <?php echo $pesanan_sudah_ditanggapi['tanggal']; ?>),
            allDay         : true,
            backgroundColor: '#00a65a', //Success (green)
            borderColor    : '#00a65a' //Success (green)
          },
        <?php
            }
          } 
        ?>
        <?php 
            if (is_array($pesanan_belum_disetujui) || is_object($pesanan_belum_disetujui)){
            foreach($pesanan_belum_disetujui as $pesanan_belum_disetujui)
            {
        ?>
          {
            title          : '<?php echo $pesanan_belum_disetujui['kd_pesanan']; ?>',
            start          : new Date(<?php echo $pesanan_belum_disetujui['tahun']; ?>,date.getMonth(<?php echo $pesanan_belum_disetujui['bulan']; ?>), <?php echo $pesanan_belum_disetujui['tanggal']; ?>),
            allDay         : true,
            backgroundColor: '#f56954', //red
            borderColor    : '#f56954' //red
          },
        <?php
            }
          } 
        ?>
      ],
      
    })
  })
</script>
<script>
  $(document).ready(function() {
      $('#example').DataTable( {
          "paging":   false,
          "ordering": false,
          "info":     false
      } );
      $('#table1').DataTable( {
          "order": [[ 3, "asc" ]]
      } );
      $('#table2').DataTable( {
          "order": [[ 3, "asc" ]]
      } );
      $('#table3').DataTable( {
          "order": [[ 4, "desc" ]]
      } );
  } );
  $(function () {
    $('#example1').DataTable()
    $('#example2').DataTable({
      'lengthChange': false,
      'searching'   : false,
      'ordering'    : true,
      'info'        : true
    })

  })
</script>
<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
    
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    CKEDITOR.replace('editor1')
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()

    //Date picker
    $('.datepicker').datepicker({
      autoclose: true,
      format: 'yyyy/mm/dd',
    });
  })
</script>
</body>
</html>