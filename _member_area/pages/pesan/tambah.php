<?php
    define('PAGE_TITLE', 'Pesan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    include_once('../../../config/controller.php');

    // Initialize the session
    session_start();
    
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 1){
        header("location:".URL_USER);
        exit;
    }
    $username = $_SESSION['username'];
    $query = "SELECT users.username,pelanggan.*,DATE_FORMAT(pelanggan.tanggal_pendaftaran, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN pelanggan ON users.id = pelanggan.id_user WHERE users.username= '".$username."'";
    $user =  selectDetail($query);

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $subyek = isset($_POST['subyek']) ? $_POST['subyek'] : "";
        $isi_pesan = isset($_POST['isi_pesan']) ? $_POST['isi_pesan'] : "";

        $id_pelanggan=$user['id_pelanggan'];
        $email = $user['email'];

        $values = array($id_pelanggan,$email,$subyek,$isi_pesan);
        $columns = array('id_pelanggan','email','subyek', 'isi_pesan');
        
        insert('pesan', $values, $columns);

        echo "<meta http-equiv='refresh' content='0;url=".URL_USER."member_area/pages/pesan'>";
    }

    
    $content_page='../../template/tambah_pesan.php';

    include_once('../../layout/main_layout.php');
?>