<?php
    define('PAGE_TITLE', 'Dashboard');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    include_once('../config/controller.php');

    // Initialize the session
    session_start();
    
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 1){
        header("location:".URL_USER);
        exit;
    }
    $username = $_SESSION['username'];
    $query = "SELECT poin_pelanggan.jumlah_poin,users.username,pelanggan.*,DATE_FORMAT(pelanggan.tanggal_pendaftaran, \"%e %M %Y %r\") AS tanggal_gabung FROM users 
                LEFT JOIN pelanggan ON users.id = pelanggan.id_user 
                LEFT JOIN poin_pelanggan ON pelanggan.id_pelanggan= poin_pelanggan.id_pelanggan
                WHERE users.username= '".$username."'";
    $user =  selectDetail($query);

    $query_pesan_masuk = "SELECT *,DATE_FORMAT(waktu_tanggapan, \"%e %M %Y %r\") AS waktu_balas,karyawan.nama_karyawan,karyawan.email as email_pengirim, karyawan.foto as foto_pengirim FROM pesan
	INNER JOIN karyawan on pesan.id_penanggap=karyawan.id_karyawan
    WHERE id_penanggap IS NOT NULL AND id_pelanggan=".$user['id_pelanggan']." ORDER BY waktu_tanggapan DESC";
    $pesan_masuk =  selectBySql($query_pesan_masuk);
    $query_jml_psn = "SELECT COUNT(id_pesan) AS jumlah_pesan_masuk FROM pesan WHERE id_penanggap IS NOT NULL AND id_pelanggan=".$user['id_pelanggan'];
    $jml_pesan =  selectDetail($query_jml_psn);

    $blog = array();
    $query_blog = "SELECT *, DATE_FORMAT(tanggal_post, \"%e %M %Y\") as tanggal_posting FROM blog";
    $blog =  selectBySql($query_blog);
    
    $content_page='../member_area/template/dashboard.php';

    include_once('../member_area/layout/main_layout.php');
?>