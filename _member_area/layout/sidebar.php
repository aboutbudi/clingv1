<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo $user['foto'] ? URL_USER.'upload/'.$user['foto'] : URL_USER.'img/profile-default.jpg' ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $user['nama_pelanggan']; ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="active">
            <a href="<?php echo URL_USER ?>" target="blank"><i class="fa fa-external-link"></i> <span>Go to Website</span></a>
        </li>
        <li>
            <a href="<?php echo URL_USER ?>member_area/"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
        </li>
        <li>
            <a href="<?php echo URL_USER ?>member_area/pages/pesan"><i class="fa fa-envelope-o"></i> <span>Pesan</span></a>
        </li>
        <li class="treeview">
            <a href="#">
              <i class="fa fa-user-o"></i> <span>Profil</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li>
                  <a href="<?php echo URL_USER ?>member_area/pages/profil"><i class="fa fa-user-o"></i> <span>Lihat Profil</span></a>
                  <a href="<?php echo URL_USER."member_area/pages/profil/ubah.php?id_pelanggan=".$user['id_pelanggan']; ?>"><i class="fa fa-pencil"></i> <span>Ubah Profil</span></a>
              </li>
            </ul>
        </li>
        <!-- <li>
            <a href="<?php echo URL_USER ?>member_area/pages/profil"><i class="fa fa-gear"></i> <span>Sistem Konfigurasi</span></a>
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>