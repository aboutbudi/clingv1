<?php 
    define('PAGE_TITLE','Register');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    include_once('../config/link.php');
    include_once('../config/controller.php');

    
    // Define variables and initialize with empty values
    $username = $password = $confirm_password = "";
    $username_err = $password_err = $confirm_password_err = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $query_last_user ="SELECT * FROM user order by id_user desc limit 1";
        $hasil_query_last_user=selectDetail($query_last_user);

        $last_record=$hasil_query_last_user['id_user'];

        $no_reg="";
        if($last_record<10){
            $no_reg= date("dmy")."000".$last_record+1;
        }elseif($last_record>=10&&$last_record<100){
            $no_reg= date("dmy")."00".$last_record+1;
        }elseif($last_record>=100){
            $no_reg= date("dmy")."0".$last_record+1;
        }else{
            $no_reg= date("dmy")."000".$last_record+1;
        }
        
        $nomor="";
        $role = isset($_POST['role']) ? $_POST['role'] : "";
        if($role=='admin'){
            $nomor="ADM".$no_reg;
        }elseif($role=='member'){
            $nomor="MBR".$no_reg;
        }

        $nama = isset($_POST['nama']) ? $_POST['nama'] : "";
        $email = isset($_POST['email']) ? $_POST['email'] : "";
        $nomor_telepon = isset($_POST['nomor_telepon']) ? $_POST['nomor_telepon'] : "";
        $tanggal_lahir = isset($_POST['tanggal_lahir']) ? $_POST['tanggal_lahir'] : "";
        $alamat = isset($_POST['alamat']) ? $_POST['alamat'] : "";
        
        $nama_file = $_FILES['foto']['name'];
        $ukuran_file = $_FILES['foto']['size'];
        $tipe_file = $_FILES['foto']['type'];
        $tmp_file = $_FILES['foto']['tmp_name'];

        $uploaddir = '../upload/';

        if(empty(trim($_POST["username"]))){
            $username_err = "Please enter a username.";
        }else{
            $sql = "SELECT id_user FROM user WHERE username = ?";

            if($stmt = msqli_prepare($link,$sql)){
                mysqli_stmt_bind_param($stmt, "s", $param_username);

                // Set parameters
                $param_username = trim($_POST["username"]);
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    /* store result */
                    mysqli_stmt_store_result($stmt);
                    
                    if(mysqli_stmt_num_rows($stmt) == 1){
                        $username_err = "This username is already taken.";
                    } else{
                        $username = trim($_POST["username"]);
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }
            // Close statement
            mysqli_stmt_close($stmt);
        }

        // Validate password
        if(empty(trim($_POST['password']))){
            $password_err = "Please enter a password.";     
        } elseif(strlen(trim($_POST['password'])) < 6){
            $password_err = "Password must have atleast 6 characters.";
        } else{
            $password = trim($_POST['password']);
        }
        
        // Validate confirm password
        if(empty(trim($_POST["confirm_password"]))){
            $confirm_password_err = 'Please confirm password.';     
        } else{
            $confirm_password = trim($_POST['confirm_password']);
            if($password != $confirm_password){
                $confirm_password_err = 'Password did not match.';
            }
        }

        // Check input errors before inserting in database
        if(empty($username_err) && empty($password_err) && empty($confirm_password_err)){
            // Prepare an insert statement
            $sql = "INSERT INTO users (username, password,role) VALUES (?, ?, ?)";
            if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "sss", $param_username, $param_password,$param_role);
                
                // Set parameters
                $param_username = $username;
                $param_password = password_hash($password, PASSWORD_DEFAULT); // Creates a password hash
                $param_role = $role;

                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    $query_user_terbuat = "SELECT * FROM user where username='".$username."'";
                    $hasil_user = selectDetail($query_user_terbuat);
                    if($nama_file==""){
                        $columns = array(
                            'nama'=>$nama,
                            'email'=>$email,
                            'nomor'=>$nomor,
                            'nomor_telepon'=>$nomor_telepon,
                            'tanggal_lahir'=>$tanggal_lahir,
                            'alamat'=>$alamat
                        );
                        
                        $condition = "id_user = ".$hasil_user['id_user'];
                        update('user',$columns, $condition);
                    }else{
                        $foto = $nomor.'-'.$nama_file;
                        $uploadfile = $uploaddir . $foto;
                        if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                            if($ukuran_file <= 1000000){
                                if(move_uploaded_file($tmp_file, $uploadfile)){
                                    $columns = array(
                                        'nama'=>$nama,
                                        'email'=>$email,
                                        'nomor'=>$nomor,
                                        'nomor_telepon'=>$nomor_telepon,
                                        'tanggal_lahir'=>$tanggal_lahir,
                                        'alamat'=>$alamat,
                                        'foto'=>$foto
                                    );
                                    
                                    $condition = "id_user = ".$hasil_user['id_user'];
                                    update('user',$columns, $condition);
                                    header("location: ".URL_ADMIN."controller/member");
                                }else{
                                    echo "<script>alert(\"File gagal diupload\");</script>";
                                }
                            }else{
                                echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                            }
                        }else{
                            echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
                        }
                    }
                }else{
                    echo "Something went wrong. Please try again later.";
                }
            }
            // Close statement
            mysqli_stmt_close($stmt);
        }
        // Close connection
        mysqli_close($link);
    }


?>