<?php 
    define('PAGE_TITLE','Register');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    include_once('../config/link.php');
    include_once('../config/controller.php');

    // Define variables and initialize with empty values
    $username = $password = "";
    $username_err = $password_err = "";

    // Processing form data when form is submitted
    if($_SERVER["REQUEST_METHOD"] == "POST"){
    
        // Check if username is empty
        if(empty(trim($_POST["username"]))){
            $username_err = 'Please enter username.';
        } else{
            $username = trim($_POST["username"]);
        }
        
        // Check if password is empty
        if(empty(trim($_POST['password']))){
            $password_err = 'Please enter your password.';
        } else{
            $password = trim($_POST['password']);
        }
        
        // Validate credentials
        if(empty($username_err) && empty($password_err)){
            // Prepare a select statement
            $sql = "SELECT id_user,role, username, password FROM user WHERE username = ?";
            
            if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "s", $param_username);
                
                // Set parameters
                $param_username = $username;
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    // Store result
                    mysqli_stmt_store_result($stmt);
                    
                    // Check if username exists, if yes then verify password
                    if(mysqli_stmt_num_rows($stmt) == 1){
                        // Bind result variables
                        mysqli_stmt_bind_result($stmt,$id_user,$role, $username, $hashed_password);
                        if(mysqli_stmt_fetch($stmt)){
                            if(password_verify($password, $hashed_password)){
                                /* Password is correct, so start a new session and
                                save the username to the session */
                                if($role=='admin'){
                                    session_start();
                                    $_SESSION['username'] = $username;
                                    $_SESSION['role'] = $role;      
                                    header("location: ".URL_ADMIN);
                                }elseif($role=='member'){
                                    session_start();
                                    $_SESSION['username'] = $username;
                                    $_SESSION['role'] = $role;      
                                    header("location: ".URL_USER."member_area");
                                }else{
                                    session_start();
                                    $_SESSION['username'] = $username;
                                    $_SESSION['role'] = $role;      
                                    header("location: ".URL_USER);
                                }
                            }else{
                                // Display an error message if password is not valid
                                $password_err = 'The password you entered was not valid.';
                            }
                        }
                    } else{
                        // Display an error message if username doesn't exist
                        $username_err = 'No account found with that username.';
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }
            // Close statement
            mysqli_stmt_close($stmt);
        }
        // Close connection
        mysqli_close($link);

    }
?>