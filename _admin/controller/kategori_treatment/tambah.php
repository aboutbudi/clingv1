<?php
    define('PAGE_TITLE', 'Kategori Perawatan > Tambah');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    define('PATH_FILE', $_SERVER["DOCUMENT_ROOT"].'/cling/upload/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $content_page='../../pages/form_kategori_perawatan.php';
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        
        $nama = isset($_POST['nama']) ? $_POST['nama'] : "";
        $deskripsi = isset($_POST['deskripsi']) ? $_POST['deskripsi'] : "";
        
        
        $values = array($nama,$deskripsi);
        $columns = array('nama', 'deskripsi');
        
        insert('kategori_perawatan', $values, $columns);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/kategori_treatment/'>";
} 
    
    

    include_once('../../layout/main_layout.php');
?>