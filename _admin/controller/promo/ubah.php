<?php
    define('PAGE_TITLE', 'Perawatan > Tambah');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $content_page='../../pages/form_promo.php';

    $perawatan = array();
    $perawatan = select('perawatan');

    $promo = array();
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        //get anggota by id 
        $id_promo = isset($_GET['id_promo']) ? $_GET['id_promo'] : "";
        $condition = 'id_promo='.$id_promo;
        $results = select('promo', null, $condition);
        $promo = count($results) > 0 ? $results[0] : array();

        $query = "SELECT * FROM perawatan WHERE id_perawatan = ".$promo['id_perawatan'];
        $nama_perawatan = selectDetail($query);
    }
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_promo = isset($_POST['id_promo']) ? $_POST['id_promo'] : "";
        $id_perawatan = isset($_POST['id_perawatan']) ? $_POST['id_perawatan'] : "";
        $nama_promo = isset($_POST['nama_promo']) ? $_POST['nama_promo'] : "";
        $deskripsi = isset($_POST['deskripsi']) ? $_POST['deskripsi'] : "";
        $temp_tanggal_mulai = isset($_POST['tanggal_mulai']) ? $_POST['tanggal_mulai'] : "";
        $temp_tanggal_selesai = isset($_POST['tanggal_selesai']) ? $_POST['tanggal_selesai'] : "";
        $potongan = isset($_POST['potongan']) ? $_POST['potongan'] : "";
        $penggunaan_poin = isset($_POST['penggunaan_poin']) ? $_POST['penggunaan_poin'] : "";
        
        $tem_mulai = strtotime($temp_tanggal_mulai);
        $tanggal_mulai = date('Y-m-d',$tem_mulai);
        $tem_selesai = strtotime($temp_tanggal_selesai);
        $tanggal_selesai = date('Y-m-d',$tem_selesai);

        $nama_file = $_FILES['foto']['name'];
        $ukuran_file = $_FILES['foto']['size'];
        $tipe_file = $_FILES['foto']['type'];
        $tmp_file = $_FILES['foto']['tmp_name'];

        $uploaddir = '../../../upload/';
        if($nama_file==""){
            $columns = array(
                'id_perawatan'=>$id_perawatan,
                'nama_promo'=>$nama_promo,
                'tanggal_mulai'=>$tanggal_mulai,
                'tanggal_selesai'=>$tanggal_selesai,
                'deskripsi'=>$deskripsi,
                'potongan'=>$potongan,
                'penggunaan_poin'=>$penggunaan_poin
            );
            
            $condition = "id_promo = ".$id_promo;
            update('promo',$columns, $condition);
            echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/promo/'>";
        }else{
            $foto = 'promo-'.$nama_file;
            $uploadfile = $uploaddir . $foto;
            if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                if($ukuran_file <= 1000000){
                    if(move_uploaded_file($tmp_file, $uploadfile)){
                        $columns = array(
                            'id_perawatan'=>$id_perawatan,
                            'nama_promo'=>$nama_promo,
                            'tanggal_mulai'=>$tanggal_mulai,
                            'tanggal_selesai'=>$tanggal_selesai,
                            'deskripsi'=>$deskripsi,
                            'potongan'=>$potongan,
                            'penggunaan_poin'=>$penggunaan_poin,
                            'foto'=>$foto
                        );
                        
                        $condition = "id_promo = ".$id_promo;
                        update('promo',$columns, $condition);
                        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/promo/'>";
                    }else{
                        echo "<script>alert(\"File gagal diupload\");</script>";
                    }
                }else{
                    echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                }
            }else{
                echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
            }
        }
    }

    include_once('../../layout/main_layout.php');
?>