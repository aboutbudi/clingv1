<?php
    define('PAGE_TITLE', 'Register');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $role = isset($_GET['role']) ? $_GET['role'] : "";
        if($role==1){
            echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/auth/register.php'>";
        }elseif($role==2){
            echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/auth/register_member.php'>";
        }
    }
?>