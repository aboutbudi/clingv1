<?php
    define('PAGE_TITLE', 'Dashboard');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    
    include_once('../../../config/link.php');

    // Define variables and initialize with empty values
    $username = $password = "";
    $username_err = $password_err = "";

    
    // Processing form data when form is submitted
    if($_SERVER["REQUEST_METHOD"] == "POST"){
    
        // Check if username is empty
        if(empty(trim($_POST["username"]))){
            $username_err = 'Please enter username.';
        } else{
            $username = trim($_POST["username"]);
        }
        
        // Check if password is empty
        if(empty(trim($_POST['password']))){
            $password_err = 'Please enter your password.';
        } else{
            $password = trim($_POST['password']);
        }
        
        // Validate credentials
        if(empty($username_err) && empty($password_err)){
            // Prepare a select statement
            $sql = "SELECT id,id_role, username, password FROM users WHERE username = ?";
            
            if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "s", $param_username);
                
                // Set parameters
                $param_username = $username;
                
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    // Store result
                    mysqli_stmt_store_result($stmt);
                    
                    // Check if username exists, if yes then verify password
                    if(mysqli_stmt_num_rows($stmt) == 1){                    
                        // Bind result variables
                        mysqli_stmt_bind_result($stmt,$id,$id_role, $username, $hashed_password);
                        if(mysqli_stmt_fetch($stmt)){
                            if(password_verify($password, $hashed_password)){
                                /* Password is correct, so start a new session and
                                save the username to the session */
                                if($id_role==1){
                                    $sql2 = "SELECT karyawan.id_karyawan FROM users JOIN karyawan ON users.id = karyawan.id_user WHERE username= ?";
                                    if($stmt2 = mysqli_prepare($link, $sql2)){
                                        mysqli_stmt_bind_param($stmt2, "s", $param_username2);
                                        $param_username2 = $username;
                                        if(mysqli_stmt_execute($stmt2)){
                                            mysqli_stmt_store_result($stmt2);
                                            if(mysqli_stmt_num_rows($stmt2) == 1){
                                                session_start();
                                                $_SESSION['username'] = $username;
                                                $_SESSION['id_role'] = $id_role;      
                                                header("location: ".URL_ADMIN);
                                            }else{
                                                session_start();
                                                $_SESSION['username'] = $username;
                                                $_SESSION['id_role'] = $id_role;      
                                                header("location: ".URL_ADMIN."controller/karyawan/tambah.php");
                                            }
                                        }else{}
                                    }else{}
                                    
                                }else{
                                    $sql2 = "SELECT pelanggan.id_pelanggan FROM users JOIN pelanggan ON users.id = pelanggan.id_user WHERE username= ?";
                                    if($stmt2 = mysqli_prepare($link, $sql2)){
                                        mysqli_stmt_bind_param($stmt2, "s", $param_username2);
                                        $param_username2 = $username;
                                        if(mysqli_stmt_execute($stmt2)){
                                            mysqli_stmt_store_result($stmt2);
                                            if(mysqli_stmt_num_rows($stmt2) == 1){
                                                session_start();
                                                $_SESSION['username'] = $username;
                                                $_SESSION['id_role'] = $id_role;      
                                                header("location: ".URL_USER."member_area/");
                                            }else{
                                                session_start();
                                                $_SESSION['username'] = $username;
                                                $_SESSION['id_role'] = $id_role;      
                                                header("location: ".URL_USER."member_area/pages/profil/tambah.php");
                                            }
                                        }else{}
                                    }else{}
                                }
                                
                            } else{
                                // Display an error message if password is not valid
                                $password_err = 'The password you entered was not valid.';
                            }
                        }
                    } else{
                        // Display an error message if username doesn't exist
                        $username_err = 'No account found with that username.';
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }
            
            // Close statement
            mysqli_stmt_close($stmt);
        }
        
        // Close connection
        mysqli_close($link);
    }
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Cling SkinCare | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo URL_USER ?>plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo URL_USER ?>index2.html">Cling SkinCare</a>
  </div>

    <div class="login-box-body">
        <h4 class="login-box-msg">Sign in to access Control Panel</h4>
        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
            <div class="form-group has-feedback <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                <input type="text" name="username" class="form-control"  value="<?php echo $username; ?>" placeholder="Username">
                <span class="fa fa-user-o form-control-feedback"></span>
                <span class="help-block"><?php echo $username_err; ?></span>
            </div>
            <div class="form-group has-feedback <?php echo (!empty($password_err)) ? 'has-error' : ''; ?>">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <span class="fa fa-key form-control-feedback"></span>
                <span class="help-block"><?php echo $password_err; ?></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
                <!-- /.col -->
            </div>
        </form>
        Don't have an account? <a href="<?php echo URL_ADMIN."controller/auth/cek.php?role=2"?>" class="text-center">Sign Up</a>
    </div>
  
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?php echo URL_USER ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo URL_USER ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?php echo URL_USER ?>plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
