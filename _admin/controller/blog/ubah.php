<?php
    define('PAGE_TITLE', 'Blog > Ubah');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');
    define('PATH_FILE', $_SERVER["DOCUMENT_ROOT"].'/cling/upload/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);
    
    $content_page='../../pages/form_blog.php';

    $blog = array();
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        //get anggota by id 
        $id_blog = isset($_GET['id_blog']) ? $_GET['id_blog'] : "";
        $condition = 'id_blog='.$id_blog;
        $results = select('blog', null, $condition);
        $blog = count($results) > 0 ? $results[0] : array();
    }

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_blog = isset($_POST['id_blog']) ? $_POST['id_blog'] : "";
        $judul = isset($_POST['judul']) ? $_POST['judul'] : "";
        $konten = isset($_POST['konten']) ? $_POST['konten'] : "";
        $penulis = isset($_POST['penulis']) ? $_POST['penulis'] : "";
        
        $nama_file = $_FILES['foto']['name'];
        $ukuran_file = $_FILES['foto']['size'];
        $tipe_file = $_FILES['foto']['type'];
        $tmp_file = $_FILES['foto']['tmp_name'];
        
        $uploaddir = '../../../upload/';
        if($nama_file==""){
            $columns = array(
                'judul'=>$judul,
                'konten'=>$konten,
                'penulis'=>$penulis
            );
            
            $condition = "id_blog = ".$id_blog;
            update('blog',$columns, $condition);
            echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/blog/'>";
        }else{
            $foto = 'blog-'.$nama_file;
            $uploadfile = $uploaddir . $foto;
            
            if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                if($ukuran_file <= 1000000){
                    if(move_uploaded_file($tmp_file, $uploadfile)){
                        $columns = array(
                            'judul'=>$judul,
                            'konten'=>$konten,
                            'penulis'=>$penulis,
                            'foto'=>$foto
                        );
                        
                        $condition = "id_blog = ".$id_blog;
                        update('blog',$columns, $condition);
                        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/blog/'>";
                    }else{
                        echo "<script>alert(\"File gagal diupload\");</script>";
                    }
                }else{
                    echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                }
            }else{
                echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
            }
        }
        
    } 
    
    

    include_once('../../layout/main_layout.php');
?>