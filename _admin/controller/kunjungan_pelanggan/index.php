<?php
    define('PAGE_TITLE', 'Kunjungn Pelanggan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    // Initialize the session
    session_start();
    
        // If session variable is not set it will redirect to login page
        if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    
    include_once('../../../config/controller.php');

    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    $kunjungan_pelanggan = array();
    $query = "SELECT c.status_transaksi,a.*,DATE_FORMAT(a.waktu_kunjungan, \"%e %M %Y %r\") AS tanggal_kunjungan,b.nomor_pelanggan,b.nama_pelanggan 
                FROM kunjungan_pelanggan a 
                INNER JOIN pelanggan b on a.id_pelanggan = b.id_pelanggan
                LEFT JOIN transaksi_kunjungan c on a.id_kunjungan_pelanggan = c.id_kunjungan 
                where c.status_transaksi IS NULL or c.status_transaksi <2 ";
    $kunjungan_pelanggan =  selectBySql($query);

    $semua_kunjungan_pelanggan = array();

    $query = "SELECT c.status_transaksi,a.*,DATE_FORMAT(a.waktu_kunjungan, \"%e %M %Y %r\") AS tanggal_kunjungan,b.nomor_pelanggan,b.nama_pelanggan 
                        FROM kunjungan_pelanggan a 
                        INNER JOIN pelanggan b on a.id_pelanggan = b.id_pelanggan
                        LEFT JOIN transaksi_kunjungan c on a.id_kunjungan_pelanggan = c.id_kunjungan 
                        where c.status_transaksi IS NOT NULL AND c.status_transaksi <>1 ORDER BY a.waktu_kunjungan DESC";
    $semua_kunjungan_pelanggan =  selectBySql($query);

    $content_page='../../pages/list_kunjungan_pelanggan.php';

    include_once('../../layout/main_layout.php');
?>