<?php
    define('PAGE_TITLE', 'Pesan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/cling/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/cling/admin/');

    include_once('../../../config/controller.php');

    // Initialize the session
    session_start();
    
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_ADMIN."controller/auth/login.php");
        exit;
    }
    if($_SESSION['id_role'] == 2){
        header("location:".URL_USER."member_area/");
        exit;
    }
    $username = $_SESSION['username'];
    $query = "SELECT users.username,karyawan.*,DATE_FORMAT(karyawan.created_at, \"%e %M %Y\") AS tanggal_gabung FROM users LEFT JOIN karyawan ON users.id = karyawan.id_user WHERE username= '".$username."'";
    $user =  selectDetail($query);

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_pesan = isset($_POST['id_pesan']) ? $_POST['id_pesan'] : "";
        $isi_tanggapan = isset($_POST['isi_tanggapan']) ? $_POST['isi_tanggapan'] : "";

        $id_penanggap=$user['id_karyawan'];

        $t=time();
        $waktu_tanggapan=date("Y-m-d H:i:s",$t);

        $columns1 = array(
            'isi_tanggapan'=>$isi_tanggapan,
            'id_penanggap'=>$id_penanggap,
            'waktu_tanggapan'=>$waktu_tanggapan
        );
        
        $condition1 = "id_pesan = ".$id_pesan;
        update('pesan',$columns1, $condition1);

        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."controller/pesan'>";
    }

    include_once('../../layout/main_layout.php');
?>