<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Member
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Member</li><li class="active">Hapus</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/member/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title">Hapus Member</h3>
                        <?php echo $pelanggan_detail['id_pelanggan'] ?>
                    </div>
                    <div class="box-body">
                        <form action="<?php echo URL_ADMIN."controller/member/hapus.php" ?>" method="post" class="form-horizontal">
                            <div class="form-group">
                                
                                <label for="nomor_pelanggan" class="col-sm-2 control-label">Nomor Pelanggan</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" id="id_pelanggan" name="id_pelanggan" value="<?php echo !empty($pelanggan_detail) ? $pelanggan_detail['id_pelanggan'] : ''; ?>">
                                    <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["nomor_pelanggan"];?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nomor_pelanggan" class="col-sm-2 control-label">Nama Pelanggan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["nama_pelanggan"];?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nomor_pelanggan" class="col-sm-2 control-label">Email</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["email"];?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nomor_pelanggan" class="col-sm-2 control-label">Nomor Telepon</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["nomor_telepon"];?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nomor_pelanggan" class="col-sm-2 control-label">Alamat</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["alamat"];?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nomor_pelanggan" class="col-sm-2 control-label">Member Sejak</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["tanggal_mendaftar"];?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nomor_pelanggan" class="col-sm-2 control-label">Jumlah Poin</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nomor_pelanggan" value="<?php echo $pelanggan_detail["jumlah_poin"];?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-10" style="color:red;text-align:right;">
                                    <h4><b>Apakah anda yakin ingin menghapus data ini ?<b></h4>
                                    
                                </div>
                                <div class="col-xs-2">
                                    <a href="<?php echo URL_ADMIN."controller/member/"?>" class="btn btn-success">Cancel</a>
                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>