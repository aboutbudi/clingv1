<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Blog
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-wordpress"></i> Home</a></li>
        <li class="active">Blog</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <a href="<?php echo URL_ADMIN."controller/blog/tambah.php"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-plus"></i> Tambah Postingan</a>
              <h3 class="box-title">List Blog</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Judul Post</th>
                  <th>Penulis</th>
                  <th>Tanggal Post</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                        if (is_array($blog) || is_object($blog)){
                        foreach($blog as $blog)
                        {
                    ?>
                  <tr>
                    <td><?php echo $blog['judul'];?></td>
                    <td><?php echo $blog['penulis'];?></td>
                    <td><?php echo $blog['tanggal_posting'];?></td>
                    <td>
                      <a href="<?php echo URL_ADMIN."controller/blog/detail.php?id_blog=".$blog['id_blog']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-list"></i></a>
                      <a href="<?php echo URL_ADMIN."controller/blog/ubah.php?id_blog=".$blog['id_blog']; ?>" class="btn btn-sm btn-warning" tooltip="view" alt="view"><i class="fa fa-pencil-square-o"></i></a>
                      <a href="<?php echo URL_ADMIN."controller/blog/hapus.php?id_blog=".$blog['id_blog']; ?>" class="btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  <?php 
                    }
                    }
                  ?>
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>