<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kategori Perawatan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Kategori Perawatan</li><li class="active">Hapus</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/kategori_treatment/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title">Hapus Kategori Perawatan</h3>
                    </div>
                    <div class="box-body">
                        <form action="<?php echo URL_ADMIN."controller/kategori_treatment/hapus.php" ?>" method="post" class="form-horizontal">
                            <div class="form-group">
                                <label for="nama" class="col-sm-2 control-label">Nama Kategori Perawatan</label>
                                <div class="col-sm-10">
                                    <input type="hidden" class="form-control" id="id_kategori_perawatan" name="id_kategori_perawatan" value="<?php echo !empty($kategori_perawatan) ? $kategori_perawatan['id_kategori_perawatan'] : ''; ?>">
                                    <input type="text" class="form-control" id="nama" value="<?php echo $kategori_perawatan["nama"];?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi" class="col-sm-2 control-label">Deskripsi</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="4" id="deskripsi" name="deskripsi" disabled><?php echo !empty($kategori_perawatan) ? $kategori_perawatan['deskripsi'] : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-10" style="color:red;text-align:right;">
                                    <h4><b>Apakah anda yakin ingin menghapus data ini ?<b></h4>
                                    
                                </div>
                                <div class="col-xs-2">
                                    <a href="<?php echo URL_ADMIN."controller/kategori_treatment/"?>" class="btn btn-success">Cancel</a>
                                    <button type="submit" class="btn btn-danger">Hapus</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>