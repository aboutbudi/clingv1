<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Member
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Member</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <a href="<?php echo URL_ADMIN."controller/member/tambah_member.php"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-plus"></i> Tambah Member</a>
              <h3 class="box-title">List Member</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Nomor Anggota</th>
                  <th>Nama Anggota</th>
                  <th>Email</th>
                  <th>Telepon </th>
                  <th>Poin </th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
											if (is_array($pelanggan) || is_object($pelanggan)){
											foreach($pelanggan as $pelanggan)
											{
									?>
                  <tr>
                    <td><?php echo $pelanggan['nomor_pelanggan'];?></td>
                    <td><?php echo $pelanggan['nama_pelanggan'];?></td>
                    <td><?php echo $pelanggan['email'];?></td>
                    <td><?php echo $pelanggan['nomor_telepon'];?> </td>
                    <td><?php echo $pelanggan['jumlah_poin'];?></td>
                    <td>
                      <a href="<?php echo URL_ADMIN."controller/member/detail.php?id_pelanggan=".$pelanggan['id_pelanggan']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-list"></i></a>
                      <a href="<?php echo URL_ADMIN."controller/member/ubah.php?id_pelanggan=".$pelanggan['id_pelanggan']; ?>" class="btn btn-sm btn-warning" tooltip="view" alt="view"><i class="fa fa-pencil-square-o"></i></a>
                      <a href="<?php echo URL_ADMIN."controller/member/hapus.php?id_pelanggan=".$pelanggan['id_pelanggan']; ?>" class="btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-trash"></i></a>
                    </td>
                  </tr>
                  <?php 
                    }
                    }
                  ?>
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>