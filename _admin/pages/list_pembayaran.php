<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pembayaran
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-shopping-bag"></i> Home</a></li>
        <li class="active">Pembayaran</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">List Tagihan</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Taggal Checkout</th>
                  <th>Nomor Kunjungan</th>
                  <th>Pelanggan</th>
                  <th>Total Tagihan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                        if (is_array($pembayaran) || is_object($pembayaran)){
                        foreach($pembayaran as $pembayaran)
                        {
                    ?>
                  <tr>
                    <td><?php echo $pembayaran['tanggal_checkout'];?></td>
                    <td><?php echo $pembayaran['nomor_kunjungan'];?></td>
                    <td><?php echo $pembayaran['nomor_pelanggan']." - ".$pembayaran['nama_pelanggan'];?></td>
                    <td style="text-align:right;"><?php echo "Rp. ".$pembayaran['total_harga'];?></td>
                    <td>
                      <a href="<?php echo URL_ADMIN."controller/pembayaran/detail.php?id_pembayaran=".$pembayaran['id_pembayaran']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-gear"></i> Proses</a>
                    </td>
                  </tr>
                  <?php 
                    }
                    }
                  ?>
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="box box-success">
            <div class="box-header">
              <h3 class="box-title">List Pembayaran Lunas</h3>
            </div>
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Taggal Pembayaran</th>
                  <th>Nomor Kunjungan</th>
                  <th>Pelanggan</th>
                  <th>Total Tagihan</th>
                  <th>Status</th>
                </tr>
                </thead>
                <tbody>
                    <?php 
                        if (is_array($pembayaran_lunas) || is_object($pembayaran_lunas)){
                        foreach($pembayaran_lunas as $pembayaran_lunas)
                        {
                    ?>
                  <tr>
                    <td><?php echo $pembayaran_lunas['tanggal_pembayaran'];?></td>
                    <td><?php echo $pembayaran_lunas['nomor_kunjungan'];?></td>
                    <td><?php echo $pembayaran_lunas['nomor_pelanggan']." - ".$pembayaran_lunas['nama_pelanggan'];?></td>
                    <td style="text-align:right;"><?php echo "Rp. ".$pembayaran_lunas['total_harga'];?></td>
                    <td>
                      <a href="<?php echo URL_ADMIN."controller/pembayaran/detail.php?id_pembayaran=".$pembayaran_lunas['id_pembayaran']; ?>" class="label label-success" tooltip="view" alt="view"><i class="fa fa-check"></i> Lunas</a>
                    </td>
                  </tr>
                  <?php 
                    }
                    }
                  ?>
                </tbody>
                
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>