<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Tulis Pesan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-envelope-o"></i> Home</a></li>
        <li>Pesan</li><li class="active">Tulis Pesan</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-3">
          
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Folders</h3>

              <div class="box-tools">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              </div>
            </div>
            <div class="box-body no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href="<?php echo URL_USER."member_area/pages/pesan/"?>"><i class="fa fa-inbox"></i> Inbox
                  <span class="label label-primary pull-right"><?php echo $jml_pesan['jumlah_pesan_masuk']>0 ? $jml_pesan['jumlah_pesan_masuk'] :'';?></span></a></li>
                <li><a href="<?php echo URL_USER."member_area/pages/pesan/terkirim.php"?>"><i class="fa fa-envelope-o"></i> Sent 
                <span class="label label-primary pull-right"><?php echo $jml_sent['jumlah_pesan_terkirim']>0 ? $jml_sent['jumlah_pesan_terkirim'] :'';?></span></a></li>
              </ul>
            </div>
            <!-- /.box-body -->
        </div>
        </div>
        <div class="col-md-9">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tulis Pesan</h3>

                    <div class="box-tools pull-right">
                        <div class="has-feedback">
                        <input type="text" class="form-control input-sm" placeholder="Search Mail">
                        <span class="glyphicon glyphicon-search form-control-feedback"></span>
                        </div>
                    </div>
                <!-- /.box-tools -->
                </div>
                <form enctype="multipart/form-data" action="<?php echo URL_USER."member_area/pages/pesan/tambah.php" ?>" method="post">
                    <div class="box-body">
                        <div class="form-group">
                            <input class="form-control" name="subyek" placeholder="Subject:">
                        </div>
                        <div class="form-group">
                            <textarea id="compose-textarea" name="isi_pesan"class="form-control textarea" style="height: 300px">
                            </textarea>
                        </div>
                    </div>
                    <div class="box-footer">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-primary"><i class="fa fa-envelope-o"></i> Send</button>
                        </div>
                        <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Discard</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>