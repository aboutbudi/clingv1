<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <section class="col-lg-7 connectedSortable">
          <div class="box box-success">
            <div class="box-header">
              <i class="fa fa-wordpress"></i>
              <h3 class="box-title">Blog Post</h3>
            </div>
            <div class="box-body chat" id="chat-box">
              <!-- post item -->
              <?php 
                  if (is_array($blog) || is_object($blog)){
                  foreach($blog as $blog)
                  {
              ?>
              <div class="item">
                <img src="<?php echo $blog['foto'] ? URL_USER."upload/".$blog['foto'] : URL_USER."images/11.jpg"?>" alt="user image" class="online">
                <p class="message">
                  <a href="<?php echo URL_ADMIN."controller/blog/detail.php?id_blog=".$blog['id_blog']; ?>" class="name">
                    <small class="text-muted pull-right"><i class="fa fa-calendar"></i> <?php echo $blog['tanggal_posting'] ?></small>
                    <?php echo $blog['judul'] ?>
                  </a>
                  Di posting oleh : <?php echo $blog['penulis'] ?>
                </p>
              </div>
              <?php 
                }
                }
              ?>
            </div>
            <div class="box-footer text-center">
              <a href="<?php echo URL_ADMIN."controller/blog" ?>" class="uppercase">Lihat Semua Post</a>
            </div>
          </div>
          
        </section>
        <section class="col-lg-5 connectedSortable">
          <div class="box box-danger">
            <div class="box-header with-border">
              <h3 class="box-title">Pelanggan Baru</h3>
              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <ul class="users-list clearfix">
                <?php 
                    if (is_array($pelanggan) || is_object($pelanggan)){
                    foreach($pelanggan as $pelanggan)
                    {
                ?>
                <li>
                  <img src="<?php echo $pelanggan['foto'] ? URL_USER."upload/".$pelanggan['foto'] : URL_USER."img/profile-default.jpg"?>" alt="User Image">
                  <a class="users-list-name" href="<?php echo URL_ADMIN."controller/member/detail.php?id_pelanggan=".$pelanggan['id_pelanggan']; ?>"><?php echo $pelanggan['nama_pelanggan'] ?></a>
                  <span class="users-list-date"><?php echo $pelanggan['tanggal_gabung'] ?></span>
                </li>
                <?php 
                  }
                  }
                ?>
              </ul>
              <!-- /.users-list -->
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
              <a href="<?php echo URL_ADMIN."controller/member" ?>" class="uppercase">Lihat Semua Pelanggan</a>
            </div>
            <!-- /.box-footer -->
          </div>
        </section>
      </div>
      <div class="row">
        <div class="col-md-4">
          <div class="box box-success direct-chat direct-chat-success">
            <div class="box-header with-border">
              <i class="fa fa-envelope-o"></i>
              <h3 class="box-title">Pesan Masuk</h3>

              <div class="box-tools pull-right">
                <span data-toggle="tooltip" title="<?php echo $jml_pesan['jumlah_pesan_masuk']?> New Messages" class="badge bg-green"><?php echo $jml_pesan['jumlah_pesan_masuk']?></span>
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <!-- Conversations are loaded here -->
              <div class="direct-chat-messages">
                <!-- Message. Default to the left -->
                <?php 
                    if (is_array($pesan_masuk) || is_object($pesan_masuk)){
                    foreach($pesan_masuk as $pesan_masuk)
                    {
                ?>
                <div class="direct-chat-msg">
                  <div class="direct-chat-info clearfix">
                    <span class="direct-chat-name pull-left"><?php echo $pesan_masuk['nama_pelanggan'] ?></span>
                    <span class="direct-chat-timestamp pull-right"><?php echo $pesan_masuk['waktu_pengiriman'] ?></span>
                  </div>
                  <!-- /.direct-chat-info -->
                  <img class="direct-chat-img" src="<?php echo $pesan_masuk['foto'] ? URL_USER."upload/".$pesan_masuk['foto'] : URL_USER."img/profile-default.jpg"?>" alt="Message User Image"><!-- /.direct-chat-img -->
                  <div class="direct-chat-text">
                    <?php echo $pesan_masuk['subyek'] ?>
                  </div>
                  <!-- /.direct-chat-text -->
                </div>
                <?php 
                  }
                  }
                ?>
                <!-- /.direct-chat-msg -->
              </div>
              <!--/.direct-chat-messages-->
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-4">
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-bath"></i>
              <h3 class="box-title">Treatment</h3>
            </div>
            <div class="box-body  chat" id="chat-box1">
              <?php 
                  if (is_array($perawatan) || is_object($perawatan)){
                  foreach($perawatan as $perawatan)
                  {
              ?>
              <div class="item">
                <img src="<?php echo $perawatan['foto'] ? URL_USER."upload/".$perawatan['foto'] : URL_USER."images/14.jpg"?>" alt="user image" class="online">
                <p class="message">
                  <a href="<?php echo URL_ADMIN."controller/perawatan/detail.php?id_perawatan=".$perawatan['id_perawatan']; ?>" class="name">
                    <small class="text-muted pull-right">Rp. <?php echo $perawatan['harga'] ?></small>
                    <?php echo $perawatan['nama_perawatan'] ?>
                  </a>
                  Kategori : <?php echo $perawatan['kategori_perawatan'] ?>
                </p>
              </div>
              <?php 
                }
                }
              ?>
            </div>
            <div class="box-footer text-center">
              <a href="<?php echo URL_ADMIN."controller/treatment" ?>" class="uppercase">Lihat Semua Perawatan</a>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="box box-warning">
            <div class="box-header">
              <i class="fa fa-xing"></i>
              <h3 class="box-title">Pengunjung Akhir-akhir ini</h3>
            </div>
            <div class="box-body chat" id="chat-box1">
              <?php 
                  if (is_array($kunjungan_pelanggan) || is_object($kunjungan_pelanggan)){
                  foreach($kunjungan_pelanggan as $kunjungan_pelanggan)
                  {
              ?>
              <div class="item">
                <img src="<?php echo $kunjungan_pelanggan['foto'] ? URL_USER."upload/".$kunjungan_pelanggan['foto'] : URL_USER."images/14.jpg"?>" alt="user image" class="online">
                <p class="message">
                  <a href="<?php echo URL_ADMIN."controller/kunjungan_pelanggan" ?>" class="name">
                    <small class="text-muted pull-right"><?php echo $kunjungan_pelanggan['nomor_kunjungan'];?></small>
                    <?php echo $kunjungan_pelanggan['nama_pelanggan'] ?>
                  </a>
                  <?php echo $kunjungan_pelanggan['tanggal_kunjungan'] ?>
                </p>
              </div>
              <?php 
                }
                }
              ?>
            </div>
            <div class="box-footer text-center">
              <a href="<?php echo URL_ADMIN."controller/kunjungan_pelanggan" ?>" class="uppercase">Lihat Semua Kunjungan</a>
            </div>
          </div>
        </div>
      </div>
    </section>
</div>