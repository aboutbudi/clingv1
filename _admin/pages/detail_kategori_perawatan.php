<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Kategori Perawatan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-shower"></i> Home</a></li>
        <li class="active">Kategori Perawatan</li><li class="active">Detail</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/kategori_treatment/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title">Detail Kategori Perawatan</h3>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="nama" class="col-sm-2 control-label">Kategori Perawatan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama" value="<?php echo $kategori_perawatan["nama"];?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nomor_pelanggan" class="col-sm-2 control-label">Deskripsi</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="4" id="deskripsi" name="deskripsi" disabled><?php echo !empty($kategori_perawatan) ? $kategori_perawatan['deskripsi'] : ''; ?></textarea>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>