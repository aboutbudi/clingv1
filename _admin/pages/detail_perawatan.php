<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Perawatan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bath"></i> Home</a></li>
        <li class="active">Perawatan</li><li class="active">Detail</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."controller/treatment/"?>" class="pull-right btn btn-sm btn-danger" tooltip="view" alt="view"><i class="fa fa-backward"></i> Kembali</a>
                        <h3 class="box-title">Detail Perawatan</h3>
                    </div>
                    <div class="box-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="kategori_perawatan" class="col-sm-2 control-label">Kategori Perawatan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="kategori_perawatan" name="kategori_perawatan" value="<?php echo !empty($perawatan) ? $perawatan['kategori_perawatan'] : ''; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_perawatan" class="col-sm-2 control-label">Nama Perawatan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama_perawatan" name="nama_perawatan" value="<?php echo !empty($perawatan) ? $perawatan['nama_perawatan'] : ''; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fasilitas" class="col-sm-2 control-label">Fasilitas</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="fasilitas" name="fasilitas" value="<?php echo !empty($perawatan) ? $perawatan['fasilitas'] : ''; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi" class="col-sm-2 control-label">deskripsi</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" rows="4" id="deskripsi" name="deskripsi" disabled><?php echo !empty($perawatan) ? $perawatan['deskripsi'] : ''; ?></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="harga" class="col-sm-2 control-label">Harga</label>
                                <label for="harga" class="col-sm-2 control-label">Rp.</label>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="harga" name="harga" value="<?php echo !empty($perawatan) ? $perawatan['harga'] : ''; ?>" disabled>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="poin" class="col-sm-2 control-label">Poin</label>
                                <div class="col-sm-10">
                                    <input type="number" class="form-control" id="poin" name="poin" value="<?php echo !empty($perawatan) ? $perawatan['poin'] : ''; ?>" disabled>
                                </div>
                            </div>
                            <!-- <div class="form-group">
                                <label for="foto" class="col-sm-2 control-label">Foto</label>
                                <div class="col-sm-10">
                                    <input name="foto" type="file" id="foto" value="<?php echo !empty($perawatan) ? $perawatan['foto'] : ''; ?>">
                                </div>
                            </div> -->
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>