<?php
    define('PAGE_TITLE', 'Home');
    define('PAGE_LOCATION', 'home');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    
    include_once('config/controller.php');

    $content_page='website/template/index_home.php';

    include_once('layout/main_layout.php');
?>