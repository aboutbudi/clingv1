<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pelanggan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bath"></i> Home</a></li>
        <li class="active">Pelanggan</li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."pelanggan"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-chevron-circle-left"></i> Kembali</a>
                        <h3 class="box-title">Detail Pelanggan</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-7">
                                <div class="form form-horizontal">
                                    <form enctype="multipart/form-data" action="<?php echo URL_ADMIN."pelanggan/edit.php"; ?>" method="post">
                                        <div class="form-group">
                                            <label for="nama_pelanggan" class="col-md-4 control-label">Nama Pelanggan</label>
                                            <div class="col-md-8">
                                                <input type="hidden" class="form-control" id="id_pelanggan" name="id_pelanggan" value="<?php echo $pelanggan['id_pelanggan']?>" >
                                                <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" value="<?php echo $pelanggan['nama_pelanggan']?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nomor_pelanggan" class="col-md-4 control-label">Nomor Pelanggan</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="nomor_pelanggan" name="nomor_pelanggan" value="<?php echo $pelanggan['nomor_pelanggan']?>"  readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="email" class="col-md-4 control-label">Email</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="email" name="email" value="<?php echo $pelanggan['email']?>"  readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="nomor_telepon" class="col-md-4 control-label">Nomor Telepon</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="nomor_telepon" name="nomor_telepon" value="<?php echo $pelanggan['nomor_telepon']?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="alamat" class="col-md-4 control-label">Alamat</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $pelanggan['alamat']?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tanggal_gabung" class="col-md-4 control-label">Tanggal Bergabung</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" id="tanggal_gabung" name="tanggal_gabung" value="<?php echo $pelanggan['tanggal_gabung']?>" readonly>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="tanggal_gabung" class="col-md-4 control-label">Foto</label>
                                            <div class="col-md-8">
                                                <input type="file" accept="image/png, image/jpeg, image/gif" class="form-control" name="foto"/> <!-- rename it -->
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align:right;">
                                                <a href="<?php echo URL_ADMIN."pelanggan/hapus.php?nomor_pelanggan=".$pelanggan['nomor_pelanggan']; ?>"class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
                                                <button type="submit" class="btn btn-warning"><i class="fa fa-pencil"></i> Update</button>
                                            </div>                                
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <img class="img-thumbnail" src="<?php echo $pelanggan['foto'] ? URL_USER."upload/".$pelanggan['foto'] : URL_USER."img/profile-default.jpg"; ?>"/>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>