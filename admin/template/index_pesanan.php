<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesanan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bath"></i> Home</a></li>
        <li class="active">Pesanan</li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <!-- <a href="#" data-toggle="modal" data-target="#new-Pesanan" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-plus"></i> Tambah Pesanan</a> -->
                        <h3 class="box-title">List Pesanan Butuh Persetujuan</h3>
                    </div>
                    <div class="box-body">
                        <table id="table2" class="display table table-bordered table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Kode Pesanan</th>
                                    <th>Nomor Pelanggan</th>
                                    <th>Nama Pelanggan</th>
                                    <th>Tanggal Pesanan</th>
                                    <th>Total Biaya</th>
                                    <th style="max-width:80px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    if (is_array($pesanan_sudah_ditanggapi) || is_object($pesanan_blm_disetujui)){
                                    foreach($pesanan_blm_disetujui as $pesanan_blm_disetujui)
                                    {
                                ?>
                                <tr>
                                    <td><?php echo $pesanan_blm_disetujui['kd_pesanan'];?></td>
                                    <td><?php echo $pesanan_blm_disetujui['nomor_pelanggan'];?></td>
                                    <td><?php echo $pesanan_blm_disetujui['nama_pelanggan'];?></td>
                                    <td><?php echo $pesanan_blm_disetujui['tanggal_pesan'];?></td>
                                    <td style="text-align:right;"><?php echo "Rp. ".$pesanan_blm_disetujui['total_bayar'];?></td>
                                    <td>
                                        <a href="<?php echo URL_ADMIN."pesanan/detail.php?kd_pesanan=".$pesanan_blm_disetujui['kd_pesanan']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-gear"></i> Options</a>
                                    </td>
                                </tr>
                                <?php 
                                    }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="box box-info">
                    <div class="box-header">
                        <a href="#" data-toggle="modal" data-target="#new-Pesanan" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-plus"></i> Tambah Pesanan</a>
                        <h3 class="box-title">List Pesanan Sudah Ditanggapi</h3>
                    </div>
                    <div class="box-body">
                        <table id="table1" class="display table table-bordered table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Kode Pesanan</th>
                                    <th>Nomor Pelanggan</th>
                                    <th>Nama Pelanggan</th>
                                    <th>Tanggal Pesanan</th>
                                    <th>Total Biaya</th>
                                    <th style="max-width:80px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    if (is_array($pesanan_sudah_ditanggapi) || is_object($pesanan_sudah_ditanggapi)){
                                    foreach($pesanan_sudah_ditanggapi as $pesanan_sudah_ditanggapi)
                                    {
                                ?>
                                <tr>
                                    <td><?php echo $pesanan_sudah_ditanggapi['kd_pesanan'];?></td>
                                    <td><?php echo $pesanan_sudah_ditanggapi['nomor_pelanggan'];?></td>
                                    <td><?php echo $pesanan_sudah_ditanggapi['nama_pelanggan'];?></td>
                                    <td><?php echo $pesanan_sudah_ditanggapi['tanggal_pesan'];?></td>
                                    <td style="text-align:right;"><?php echo "Rp. ".$pesanan_sudah_ditanggapi['total_bayar'];?></td>
                                    <td>
                                        <a href="<?php echo URL_ADMIN."pesanan/detail.php?kd_pesanan=".$pesanan_sudah_ditanggapi['kd_pesanan']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-gear"></i> Options</a>
                                    </td>
                                </tr>
                                <?php 
                                    }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>