<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pesanan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bath"></i> Home</a></li>
        <li>Pesanan</li><li class="active">Detail Pesanan</li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."pesanan"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-cevron-left"></i> Kembali</a>
                        <h3 class="box-title">Detail Pesanan</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="small-box bg-aqua">
                                    <div class="inner">
                                        <table>
                                            <tbody style="color:#fff;font-weight: bold;">
                                                <tr>
                                                    <td style="width:140px;"><h4>Kode Pesanan  </h4></td>
                                                    <td><h4><strong><?php echo $pesanan['kd_pesanan'] ?></strong></h4></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:140px;"><h4>Pesanan Dari</h4></td>
                                                    <td><h4><strong><?php echo $pesanan['nama_pelanggan']." - ".$pesanan['nomor_pelanggan'] ;?></strong></h4></td>
                                                </tr> 
                                                <tr>
                                                    <td style="width:140px;"><h4>Tanggal Pesanan  </h4></td>
                                                    <td><h4><strong><?php echo $pesanan['tanggal_pesan'] ?></strong></h4></td>
                                                </tr>   
                                            </tbody>
                                        </table>
                                        <br><br>
                                    </div>
                                    <div class="icon" style="margin-top:15px;">
                                        <i class="fa fa-user-o"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                            <div class="col-md-4">
                            <div class="small-box bg-aqua">
                                <div class="inner">
                                    <table>
                                        <tbody style="color:#fff;font-weight: bold;">
                                            <tr>
                                                <td style="width:200px;"><h4>Total Bayar Awal  </h4></td>
                                                <td style="text-align:right;"><h4><strong><?php echo $biaya['total_bayar_seharusnya'] ? "Rp. ".$biaya['total_bayar_seharusnya'] : "-" ?></strong></h4></td>
                                            </tr>
                                            <tr>
                                                <td style="width:200px;"><h4>Belum Disetujui  </h4></td>
                                                <td style="text-align:right;"><h4><strong><?php echo $biaya['total_bayar_belum_disetujui'] ? "Rp. ".$biaya['total_bayar_belum_disetujui'] :"-" ?></strong></h4></td>
                                            </tr>
                                            <tr>
                                                <td style="width:200px;"><h4>Ditolak</h4></td>
                                                <td style="text-align:right;"><h4><strong><?php echo $biaya['total_bayar_ditolak'] ? "Rp. ".$biaya['total_bayar_ditolak'] : "-" ?></strong></h4></td>
                                            </tr> 
                                            <tr>
                                                <td style="width:200px;"><h4>Total Bayar Final  </h4></td>
                                                <td style="text-align:right;"><h4><strong><?php echo $biaya['total_bayar_disetujui'] ? "Rp. ".$biaya['total_bayar_disetujui'] : "-" ?></strong></h4></td>
                                            </tr>   
                                        </tbody>
                                    </table>
                                </div>
                                <div class="icon"style="margin-top:15px;">
                                    <i class="fa fa-shopping-basket"></i>
                                </div>
                            </div>
                            </div>
                        </div>
                        <table id="example" class="display table table-bordered table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr style="text-align:center;">
                                    <th>Perawatan</th>
                                    <th>Harga</th>
                                    <th style="max-width:140px"> Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    if (is_array($detail_pesanan) || is_object($detail_pesanan)){
                                    foreach($detail_pesanan as $detail_pesanan)
                                    {
                                ?>
                                <tr>
                                    <td><?php echo $detail_pesanan['nama_perawatan']?></td>
                                    <td>
                                        <?php echo "Rp. ".$detail_pesanan['harga']?></td>
                                    <td>
                                    <?php
                                        if($detail_pesanan['status']=='belum-disetujui'){
                                    ?>
                                        <a href="<?php echo URL_ADMIN."pesanan/setujui.php?id_detail_pesanan=".$detail_pesanan['id_detail_pesanan']; ?>" class="btn btn-sm btn-success"><i class="fa fa-check"></i> setujui</a>
                                        <a href="<?php echo URL_ADMIN."pesanan/tolak.php?id_detail_pesanan=".$detail_pesanan['id_detail_pesanan']; ?>" class="btn btn-sm btn-danger"><i class="fa fa-times"></i> tolak</a>
                                    <?php
                                        }elseif($detail_pesanan['status']=='disetujui'){
                                    ?>
                                        <span class="label label-success">Disetujui</span>
                                    <?php
                                        }elseif($detail_pesanan['status']=='ditolak'){
                                    ?>
                                        <span class="label label-danger">Ditolak</span>
                                    <?php
                                        } 
                                    ?>
                                    </td>
                                </tr>
                                <?php 
                                    }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>