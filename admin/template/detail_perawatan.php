<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Perawatan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bath"></i> Home</a></li>
        <li class="active">Perawatan</li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."perawatan"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-chevron-circle-left"></i> Kembali</a>
                        <h3 class="box-title">Detail Perawatan</h3>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-3">
                            </div>
                            <div class="col-md-6">
                                <div class="form form-horizontal">
                                    <form action="<?php echo URL_ADMIN."perawatan/edit.php"; ?>" method="post">
                                        <div class="form-group">
                                            <label for="nama_perawatan" class="col-md-3 control-label">Nama Perawatan</label>
                                            <div class="col-md-9">
                                                <input type="hidden" class="form-control" id="id_perawatan" name="id_perawatan" value="<?php echo $perawatan['id_perawatan']?>" >
                                                <input type="text" class="form-control" id="nama_perawatan" name="nama_perawatan" value="<?php echo $perawatan['nama_perawatan']?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="harga" class="col-md-3 control-label">Harga</label>
                                            <label for="harga" class="col-md-1 control-label">Rp. </label>
                                            <div class="col-md-8">
                                                <input type="number" class="form-control" id="harga" name="harga" value="<?php echo $perawatan['harga']?>" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-12" style="text-align:right;">
                                                <a href="<?php echo URL_ADMIN."perawatan/hapus.php?id_perawatan=".$perawatan['id_perawatan']; ?>"class="btn btn-danger"><i class="fa fa-trash-o"></i> Delete</a>
                                                <button type="submit" class="btn btn-warning"><i class="fa fa-pencil"></i> Update</button>
                                            </div>                                
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>