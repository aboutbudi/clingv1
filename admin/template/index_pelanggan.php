<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pelanggan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bath"></i> Home</a></li>
        <li class="active">Pelanggan</li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="<?php echo URL_ADMIN."pelanggan/tambah.php"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-plus"></i> Tambah Pelanggan</a>
                        <h3 class="box-title">List Pelanggan</h3>
                    </div>
                    <div class="box-body">
                    <table id="table3" class="display table table-bordered table-striped dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nomor </th>
                                    <th>Nama </th>
                                    <th>Email</th>
                                    <th>Telepon</th>
                                    <th>Tanggal Daftar</th>
                                    <th style="max-width:80px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    if (is_array($pelanggan) || is_object($pelanggan)){
                                    foreach($pelanggan as $pelanggan)
                                    {
                                ?>
                                <tr>
                                    <td><?php echo $pelanggan['nomor_pelanggan'];?></td>
                                    <td><?php echo $pelanggan['nama_pelanggan'];?></td>
                                    <td><?php echo $pelanggan['email'];?></td>
                                    <td><?php echo $pelanggan['nomor_telepon'];?></td>
                                    <td><?php echo $pelanggan['tanggal_gabung'];?></td>
                                    <td>
                                        <a href="<?php echo URL_ADMIN."pelanggan/detail.php?nomor_pelanggan=".$pelanggan['nomor_pelanggan']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-gear"></i> Options</a>
                                    </td>
                                </tr>
                                <?php 
                                    }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>