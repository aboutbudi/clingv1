<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <section class="col-lg-8 connectedSortable">
                <div class="box box-primary">
                    <div class="box-body no-padding">
                    <!-- THE CALENDAR -->
                        <div id="kalender"></div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </section>
            <section class="col-lg-4 connectedSortable">
                <div class="box box-danger">
                    <div class="box-header with-border">
                    <i class="fa fa-users"></i>
                    <h3 class="box-title">Pelanggan Baru</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                        </button>
                    </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">
                    <ul class="users-list clearfix">
                        <?php 
                            if (is_array($pelanggan) || is_object($pelanggan)){
                            foreach($pelanggan as $pelanggan)
                            {
                        ?>
                        <li>
                        <img class="img-thumnail" style="width:60px;height:60px;"  src="<?php echo $pelanggan['foto'] ? URL_USER."upload/".$pelanggan['foto'] : URL_USER."img/profile-default.jpg"?>" alt="User Image">
                        <a class="users-list-name" href="<?php echo URL_ADMIN."controller/member/detail.php?id_pelanggan=".$pelanggan['id_pelanggan']; ?>"><?php echo $pelanggan['nama_pelanggan'] ?></a>
                        <span class="users-list-date"><?php echo $pelanggan['tanggal_gabung'] ?></span>
                        </li>
                        <?php 
                        }
                        }
                        ?>
                    </ul>
                    <!-- /.users-list -->
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer text-center">
                    <a href="<?php echo URL_ADMIN."pelanggan" ?>" class="uppercase">Lihat Semua Pelanggan</a>
                    </div>
                    <!-- /.box-footer -->

                </div>
                <div class="box box-info">
                    <div class="box-header">
                    <i class="fa fa-bath"></i>
                    <h3 class="box-title">Perawatan</h3>
                    </div>
                    <div class="box-body  chat" id="chat-box1">
                    <?php 
                        if (is_array($perawatan) || is_object($perawatan)){
                        foreach($perawatan as $perawatan)
                        {
                    ?>
                    <div class="item">
                        <img src="<?php echo $perawatan['foto'] ? URL_USER."upload/".$perawatan['foto'] : URL_USER."images/14.jpg"?>" alt="user image" class="online">
                        <p class="message">
                        <a href="<?php echo URL_ADMIN."controller/perawatan/detail.php?id_perawatan=".$perawatan['id_perawatan']; ?>" class="name">
                            <small class="text-muted pull-right">Rp. <?php echo $perawatan['harga'] ?></small>
                            <?php echo $perawatan['nama_perawatan'] ?>
                        </a>
                        <br>
                        </p>
                    </div>
                    <?php 
                        }
                        }
                    ?>
                    </div>
                    <div class="box-footer text-center">
                    <a href="<?php echo URL_ADMIN."perawatan" ?>" class="uppercase">Lihat Semua Perawatan</a>
                    </div>
                </div>
            </section>
        </div>
    </section>
</div>