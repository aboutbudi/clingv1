<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Perawatan
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-bath"></i> Home</a></li>
        <li class="active">Perawatan</li>
      </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <div class="box-header">
                        <a href="#" data-toggle="modal" data-target="#new-perawatan" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-plus"></i> Tambah Perawatan</a>
                        <h3 class="box-title">List Perawatan</h3>
                    </div>
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped table-responsive dt-responsive nowrap" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Nama Perawatan</th>
                                    <th>Harga</th>
                                    <th style="max-width:80px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    if (is_array($perawatan) || is_object($perawatan)){
                                    foreach($perawatan as $perawatan)
                                    {
                                ?>
                                <tr>
                                    <td><?php echo $perawatan['nama_perawatan'];?></td>
                                    <td style="text-align:right;"><?php echo "Rp. ".$perawatan['harga'];?></td>
                                    <td>
                                        <a href="<?php echo URL_ADMIN."perawatan/detail.php?id_perawatan=".$perawatan['id_perawatan']; ?>" class="btn btn-sm btn-info" tooltip="view" alt="view"><i class="fa fa-gear"></i> Options</a>
                                    </td>
                                </tr>
                                <?php 
                                    }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal fade" id="new-perawatan">
    <div class="modal-dialog">
        <div class="modal-content">
                <form enctype="multipart/form-data" action="<?php echo URL_ADMIN."perawatan/tambah.php" ?>" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Perawatan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="nama_perawatan" class="col-md-4 control-label">Nama Perawatan</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="nama_perawatan" name="nama_perawatan" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="harga" class="col-md-4 control-label">Harga</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="harga" name="harga" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="foto" class="col-md-4 control-label">Foto</label>
                            <div class="col-md-8">
                                <input type="file" accept="image/png, image/jpeg, image/gif" class="form-control" name="foto"/> <!-- rename it -->
                            </div>
                        </div>
                    </div>
                   
                    <br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Tambah</button>
                </div>
            </form> 
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>