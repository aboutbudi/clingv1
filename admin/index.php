<?php
    define('PAGE_TITLE', 'Home');
    define('PAGE_LOCATION', 'home');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    
    include_once('../config/controller.php');

    $pelanggan = array();
    $query = "SELECT *,DATE_FORMAT(tanggal_pendaftaran, \"%e %M %Y\") as tanggal_gabung FROM pelanggan ORDER BY tanggal_pendaftaran desc LIMIT 8";
    $pelanggan =  selectBySql($query);

    $perawatan = array();
    $query2 = "SELECT * FROM perawatan LIMIT 5";
    $perawatan =  selectBySql($query2);
    
    $pesanan_sudah_ditanggapi = array();
    $query1 = "SELECT a.kd_pesanan, DATE_FORMAT(a.tanggal_pesanan, \"%e\") as tanggal,DATE_FORMAT(a.tanggal_pesanan, \"%c\") as bulan, DATE_FORMAT(a.tanggal_pesanan, \"%Y\") as tahun
                FROM pesanan a
                INNER JOIN detail_pesanan b on a.kd_pesanan = b.kd_pesanan
                INNER JOIN pelanggan c on a.nomor_pelanggan = c.nomor_pelanggan
                where b.status <> 'belum-disetujui' and b.status <> 'belum-submit'
                group by a.kd_pesanan";
    $pesanan_sudah_ditanggapi =  selectBySql($query1);

    $pesanan_belum_disetujui = array();
    $query3 = "SELECT a.kd_pesanan, DATE_FORMAT(a.tanggal_pesanan, \"%e\") as tanggal,DATE_FORMAT(a.tanggal_pesanan, \"%c\") as bulan, DATE_FORMAT(a.tanggal_pesanan, \"%Y\") as tahun
                FROM pesanan a
                INNER JOIN detail_pesanan b on a.kd_pesanan = b.kd_pesanan
                INNER JOIN pelanggan c on a.nomor_pelanggan = c.nomor_pelanggan
                where b.status = 'belum-disetujui' group by a.kd_pesanan";
    $pesanan_belum_disetujui =  selectBySql($query3);


    $content_page='template/index_home.php';

    include_once('../layout_admin/main_layout.php');
?>