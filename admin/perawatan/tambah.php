<?php
    define('PAGE_TITLE', 'Perawatan');
    define('PAGE_LOCATION', 'perawatan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    
    include_once('../../config/controller.php');

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $nama_perawatan = isset($_POST['nama_perawatan']) ? $_POST['nama_perawatan'] : "";
        $harga = isset($_POST['harga']) ? $_POST['harga'] : "";

        $nama_file = $_FILES['foto']['name'];
        $ukuran_file = $_FILES['foto']['size'];
        $tipe_file = $_FILES['foto']['type'];
        $tmp_file = $_FILES['foto']['tmp_name'];
        
        $uploaddir = '../../upload/';
        if($nama_file==""){
            $values = array($nama_perawatan,$harga);
            $columns = array('nama_perawatan','harga');
            
            insert('perawatan', $values, $columns);
            echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."perawatan/'>";
        }else{
            $foto = 'treatment-'.$nama_file;
            $uploadfile = $uploaddir . $foto;
            if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                if($ukuran_file <= 1000000){
                    if(move_uploaded_file($tmp_file, $uploadfile)){
                        $values = array($nama_perawatan,$harga,$foto);
                        $columns = array('nama_perawatan','harga','foto');
                        
                        insert('perawatan', $values, $columns);
                        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."perawatan/'>";
                    }else{
                        echo "<script>alert(\"File gagal diupload\");</script>";
                    }
                }else{
                    echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                }
            }else{
                echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
            }
        }
    }

    $content_page='../template/index_perawatan.php';

    include_once('../../layout_admin/main_layout.php');
?>