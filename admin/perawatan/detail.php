<?php
    define('PAGE_TITLE', 'Perawatan');
    define('PAGE_LOCATION', 'perawatan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    
    include_once('../../config/controller.php');

    $id_perawatan  = $_GET["id_perawatan"];
    $query = "SELECT * FROM perawatan where id_perawatan =".$id_perawatan;
    $perawatan = selectDetail($query);

    $content_page='../template/detail_perawatan.php';

    include_once('../../layout_admin/main_layout.php');
?>