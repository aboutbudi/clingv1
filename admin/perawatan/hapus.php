<?php
    define('PAGE_TITLE', 'Perawatan');
    define('PAGE_LOCATION', 'perawatan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    
    include_once('../../config/controller.php');
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $id_perawatan = isset($_GET['id_perawatan']) ? $_GET['id_perawatan'] : "";

        $condition = "id_perawatan = ".$id_perawatan;
        delete('perawatan', $condition);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."perawatan/'>";
    }
?>