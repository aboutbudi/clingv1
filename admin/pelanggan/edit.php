<?php
    define('PAGE_TITLE', 'Pelanggan');
    define('PAGE_LOCATION', 'pelanggan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    
    include_once('../../config/controller.php');

    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $nomor_pelanggan = isset($_POST['nomor_pelanggan']) ? $_POST['nomor_pelanggan'] : "";
        $nama_pelanggan = isset($_POST['nama_pelanggan']) ? $_POST['nama_pelanggan'] : "";
        $nomor_telepon = isset($_POST['nomor_telepon']) ? $_POST['nomor_telepon'] : "";
        $alamat = isset($_POST['alamat']) ? $_POST['alamat'] : "";

        echo " nomor_pelanggan = ".$nomor_pelanggan;
        echo " nama_pelanggan = ".$nama_pelanggan;
        echo " nomor_telepon = ".$nomor_telepon;
        echo " alamat = ".$alamat;

        $nama_file = $_FILES['foto']['name'];
        $ukuran_file = $_FILES['foto']['size'];
        $tipe_file = $_FILES['foto']['type'];
        $tmp_file = $_FILES['foto']['tmp_name'];
        
        $uploaddir = '../../upload/';

        if($nama_file==""){
            $columns = array(
                'nama_pelanggan'=>$nama_pelanggan,
                'nomor_telepon'=>$nomor_telepon,
                'alamat'=>$alamat
            );
            $condition = "nomor_pelanggan = '".$nomor_pelanggan."'";
            update('pelanggan',$columns, $condition);
            echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."pelanggan/'>";
        }else{
            $foto = 'pelanggan-'.$nama_file;
            $uploadfile = $uploaddir . $foto;
            if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                if($ukuran_file <= 1000000){
                    if(move_uploaded_file($tmp_file, $uploadfile)){
                        $columns = array(
                            'nama_pelanggan'=>$nama_pelanggan,
                            'nomor_telepon'=>$nomor_telepon,
                            'alamat'=>$alamat,
                            'foto'=>$foto
                        );
                        
                        $condition = "nomor_pelanggan = '".$nomor_pelanggan."'";
                        update('pelanggan',$columns, $condition);
                        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."pelanggan/'>";
                    }else{
                        echo "<script>alert(\"File gagal diupload\");</script>";
                    }
                }else{
                    echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                }
            }else{
                echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
            }
        }
    }
?>