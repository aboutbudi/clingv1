<?php
    define('PAGE_TITLE', 'Pelanggan');
    define('PAGE_LOCATION', 'pelanggan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    include_once('../../config/link.php');
    include_once('../../config/controller.php');

    // Define variables and initialize with empty values
    $email = $nama_pelanggan = $nomor_telepon = $alamat = "";
    $email_err = "";
    
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        
        if(empty(trim($_POST["email"]))){
            $email_err = "Please enter a email.";
        } else{
            // Prepare a select statement
            $sql = "SELECT nomor_pelanggan FROM pelanggan where email= ?";
            
            if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "s", $param_email);
                
                // Set parameters
                $param_email = trim($_POST["email"]);
                
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    /* store result */
                    mysqli_stmt_store_result($stmt);
                    
                    if(mysqli_stmt_num_rows($stmt) == 1){
                        $email_err = "This email is already taken.";
                        $email = trim($_POST["email"]);
                        $nama_pelanggan = trim($_POST["nama_pelanggan"]);
                        $nomor_telepon = trim($_POST["nomor_telepon"]);
                        $alamat = trim($_POST["alamat"]);
                    } else{
                        $email = trim($_POST["email"]);
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }
            
            // Close statement
            mysqli_stmt_close($stmt);
        }
        
        //Check input errors before inserting in database
        if(empty($email_err)){
            echo "Masuk no error ";
            // Prepare an insert statement
            $nama_pelanggan = isset($_POST['nama_pelanggan']) ? $_POST['nama_pelanggan'] : "";
            $nomor_telepon = isset($_POST['nomor_telepon']) ? $_POST['nomor_telepon'] : "";
            $alamat = isset($_POST['alamat']) ? $_POST['alamat'] : "";
            
            $query_last_member ="SELECT * FROM pelanggan order by nomor_pelanggan desc limit 1";
            $hasil_query_last_member=selectDetail($query_last_member);

            $_last_record=substr($hasil_query_last_member['nomor_pelanggan'], -1);
            $last_record=(int)$_last_record;

            $no_reg="";
            if($last_record<10){
                $no_reg= date("dmy")."000".$last_record+1;
            }elseif($last_record>=10&&$last_record<100){
                $no_reg= date("dmy")."00".$last_record+1;
            }elseif($last_record>=100){
                $no_reg= date("dmy")."0".$last_record+1;
            }

            $nomor="MBR".$no_reg;
            $foto="";

            $nama_file = $_FILES['foto']['name'];
            $ukuran_file = $_FILES['foto']['size'];
            $tipe_file = $_FILES['foto']['type'];
            $tmp_file = $_FILES['foto']['tmp_name'];

            $uploaddir = '../../upload/';
            if($nama_file==""){
                $values = array($nama_pelanggan,$email,$nomor_telepon,$alamat,$nomor);
                $columns = array('nama_pelanggan', 'email', 'nomor_telepon','alamat','nomor_pelanggan');
                
                insert('pelanggan', $values, $columns);
                echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."pelanggan'>";
            }else{
                $foto = 'pelanggan-'.$nama_file;
                $uploadfile = $uploaddir . $foto;
                if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                    if($ukuran_file <= 1000000){
                        if(move_uploaded_file($tmp_file, $uploadfile)){
                            $values = array($nama_pelanggan,$email,$nomor_telepon,$alamat,$nomor,$foto);
                            $columns = array('nama_pelanggan', 'email', 'nomor_telepon','alamat','nomor_pelanggan','foto');
                            
                            insert('pelanggan', $values, $columns);
                            echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."pelanggan'>";
                        }else{
                            echo "<script>alert(\"File gagal diupload\");</script>";
                        }
                    }else{
                        echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                    }
                }else{
                    echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
                }
            }
        }
    }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo 'Cling SkinCare Admin | '.PAGE_TITLE;?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/Ionicons/css/ionicons.min.css">
        <!-- DataTables -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/datatable-FixedHeader/css/fixedHeader.bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/datatable-responsive/css/responsive.bootstrap.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>css/skins/_all-skins.min.css">
        <!-- Morris chart -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/morris.js/morris.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/jvectormap/jquery-jvectormap.css">
        <!-- Date Picker -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/select2/dist/css/select2.min.css">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style>
            .example-modal .modal {
            position: relative;
            top: auto;
            bottom: auto;
            right: auto;
            left: auto;
            display: block;
            z-index: 1;
            }

            .example-modal .modal {
            background: transparent !important;
            }
        </style>
    </head>
    <body class="hold-transition skin-purple sidebar-mini">
        <div class="wrapper">
            <?php
                include '../../layout_admin/header.php';
                include '../../layout_admin/sidebar.php';
            ?>
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                <h1>
                    Pelanggan
                    <small>Control panel</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-bath"></i> Home</a></li>
                    <li class="active">Pelanggan</li>
                </ol>
                </section>
                <section class="content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-info">
                                <div class="box-header">
                                    <a href="<?php echo URL_ADMIN."pelanggan"?>" class="pull-right btn btn-sm btn-success" tooltip="view" alt="view"><i class="fa fa-chevron-circle-left"></i> Kembali</a>
                                    <h3 class="box-title">Daftar Pelanggan</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-1">
                                        </div>
                                        <div class="col-md-7">
                                            <div class="form form-horizontal">
                                                <form enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                                                    <div class="form-group">
                                                        <label for="nama_pelanggan" class="col-md-4 control-label">Nama Lengkap</label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" value="<?php echo $nama_pelanggan; ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                                                        <label for="email" class="col-md-4 control-label">Email</label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="email" name="email" value="<?php echo $email; ?>"  required>
                                                            <span class="help-block"><?php echo $email_err; ?></span>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="nomor_telepon" class="col-md-4 control-label">Nomor Telepon</label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="nomor_telepon" name="nomor_telepon" value="<?php echo $nomor_telepon; ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="alamat" class="col-md-4 control-label">Alamat</label>
                                                        <div class="col-md-8">
                                                            <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $alamat; ?>" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="tanggal_gabung" class="col-md-4 control-label">Foto</label>
                                                        <div class="col-md-8">
                                                            <input type="file" accept="image/png, image/jpeg, image/gif" class="form-control" name="foto"/> <!-- rename it -->
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="col-md-12" style="text-align:right;">
                                                            <button type="submit" class="btn btn-warning"><i class="fa fa-pencil"></i> Tambah</button>
                                                        </div>                                
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <?php
                include '../../layout_admin/footer.php';
            ?>
        </div>
        <!-- jQuery 3 -->
        <script src="<?php echo URL_USER ?>bower_components/jquery/dist/jquery.min.js"></script>
        <!-- jQuery UI 1.11.4 -->
        <script src="<?php echo URL_USER ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
        $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Bootstrap 3.3.7 -->
        <script src="<?php echo URL_USER ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Morris.js charts -->
        <script src="<?php echo URL_USER ?>bower_components/raphael/raphael.min.js"></script>
        <script src="<?php echo URL_USER ?>bower_components/morris.js/morris.min.js"></script>
        <!-- Sparkline -->
        <script src="<?php echo URL_USER ?>bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
        <!-- jvectormap -->
        <script src="<?php echo URL_USER ?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="<?php echo URL_USER ?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
        <!-- jQuery Knob Chart -->
        <script src="<?php echo URL_USER ?>bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
        <!-- InputMask -->
        <script src=".<?php echo URL_USER ?>plugins/input-mask/jquery.inputmask.js"></script>
        <script src=".<?php echo URL_USER ?>plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <script src=".<?php echo URL_USER ?>plugins/input-mask/jquery.inputmask.extensions.js"></script>
        <!-- daterangepicker -->
        <script src="<?php echo URL_USER ?>bower_components/moment/min/moment.min.js"></script>
        <script src="<?php echo URL_USER ?>bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
        <!-- datepicker -->
        <script src="<?php echo URL_USER ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <!-- Select2 -->
        <script src="<?php echo URL_USER ?>bower_components/select2/dist/js/select2.full.min.js"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="<?php echo URL_USER ?>plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo URL_USER ?>bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo URL_USER ?>bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="<?php echo URL_USER ?>bower_components/datatable-FixedHeader/js/dataTables.fixedHeader.min.js"></script>
        <script src="<?php echo URL_USER ?>bower_components/datatable-responsive/js/dataTables.responsive.min.js"></script>
        <script src="<?php echo URL_USER ?>bower_components/datatable-responsive/js/responsive.bootstrap.min.js"></script>
        <!-- Slimscroll -->
        <script src="<?php echo URL_USER ?>bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="<?php echo URL_USER ?>bower_components/fastclick/lib/fastclick.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo URL_USER ?>js/adminlte.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <script src="<?php echo URL_USER ?>js/pages/dashboard.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo URL_USER ?>js/demo.js"></script>

        <!-- page script -->
        <script>
        $(function () {
            $('#example1').DataTable()
            $('#example2').DataTable({
            'paging'      : true,
            'lengthChange': false,
            'searching'   : false,
            'ordering'    : true,
            'info'        : true,
            'autoWidth'   : false
            })

        })
        </script>
        <script>
        $(function () {
            //Initialize Select2 Elements
            $('.select2').select2()
            
            // Replace the <textarea id="editor1"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('editor1')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()

            //Date picker
            $('.datepicker').datepicker({
            autoclose: true,
            format: 'yyyy/mm/dd',
            });
        })
        </script>    
    </body>
</html>