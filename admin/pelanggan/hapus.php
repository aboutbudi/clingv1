<?php
    define('PAGE_TITLE', 'Pelanggan');
    define('PAGE_LOCATION', 'pelanggan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    
    include_once('../../config/controller.php');
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $nomor_pelanggan = isset($_GET['nomor_pelanggan']) ? $_GET['nomor_pelanggan'] : "";

        $condition = "nomor_pelanggan = '".$nomor_pelanggan."'";
        delete('pelanggan', $condition);
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."pelanggan/'>";
    }
?>