<?php
    define('PAGE_TITLE', 'Pesanan');
    define('PAGE_LOCATION', 'pesanan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    
    include_once('../../config/controller.php');


    $kd_pesanan  = $_GET["kd_pesanan"];
    $query = "SELECT *,DATE_FORMAT(a.tanggal_pesanan, \"%e %M %Y\") as tanggal_pesan FROM pesanan a 
            INNER JOIN pelanggan b on a.nomor_pelanggan = b.nomor_pelanggan 
            WHERE kd_pesanan ='".$kd_pesanan."'";
    $pesanan = selectDetail($query);

    $detail_pesanan = array();
    $query1 = "SELECT a.*,b.nama_perawatan FROM detail_pesanan a
            INNER JOIN perawatan b on a.id_perawatan = b.id_perawatan 
            where a.status <> 'belum-submit' and a.kd_pesanan ='".$kd_pesanan."'";
    $detail_pesanan =  selectBySql($query1);

    $query2="select a.kd_pesanan, sum(a.harga) as total_bayar_seharusnya, 
            b.total_bayar as total_bayar_belum_disetujui,
            c.total_bayar as total_bayar_disetujui,
            d.total_bayar as total_bayar_ditolak 
            from detail_pesanan a
            left join (select kd_pesanan, sum(harga) as total_bayar from detail_pesanan where status='belum-disetujui' and kd_pesanan='".$kd_pesanan."') b on a.kd_pesanan=b.kd_pesanan
            left join (select kd_pesanan, sum(harga) as total_bayar from detail_pesanan where status='disetujui' and kd_pesanan='".$kd_pesanan."') c on a.kd_pesanan=c.kd_pesanan
            left join (select kd_pesanan, sum(harga) as total_bayar from detail_pesanan where status='ditolak' and kd_pesanan='".$kd_pesanan."') d on a.kd_pesanan=d.kd_pesanan
            where a.status <>'belum-submit' and a.kd_pesanan='".$kd_pesanan."'";
    $biaya = selectDetail($query2);
    
    $content_page='../template/detail_pesanan.php';
    
    include_once('../../layout_admin/main_layout.php');
?>