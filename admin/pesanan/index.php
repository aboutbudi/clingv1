<?php
    define('PAGE_TITLE', 'Pesanan');
    define('PAGE_LOCATION', 'pesanan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    
    include_once('../../config/controller.php');

    $pesanan_blm_disetujui = array();
    $query = "SELECT a.kd_pesanan,a.tanggal_pesanan,c.nama_pelanggan,c.nomor_pelanggan,a.total_bayar,DATE_FORMAT(a.tanggal_pesanan, \"%e %M %Y\") as tanggal_pesan
                FROM pesanan a
                INNER JOIN detail_pesanan b on a.kd_pesanan = b.kd_pesanan
                INNER JOIN pelanggan c on a.nomor_pelanggan = c.nomor_pelanggan
                where b.status = 'belum-disetujui' group by a.kd_pesanan";
    $pesanan_blm_disetujui =  selectBySql($query);
    
    $pesanan_sudah_ditanggapi = array();
    $query1 = "SELECT a.kd_pesanan,a.tanggal_pesanan,c.nama_pelanggan,c.nomor_pelanggan,a.total_bayar,DATE_FORMAT(a.tanggal_pesanan, \"%e %M %Y\") as tanggal_pesan
                FROM pesanan a
                INNER JOIN detail_pesanan b on a.kd_pesanan = b.kd_pesanan
                INNER JOIN pelanggan c on a.nomor_pelanggan = c.nomor_pelanggan
                where b.status <> 'belum-disetujui' and b.status <> 'belum-submit'
                group by a.kd_pesanan";
    $pesanan_sudah_ditanggapi =  selectBySql($query1);
    
    
    $content_page='../template/index_pesanan.php';
    
    include_once('../../layout_admin/main_layout.php');
?>