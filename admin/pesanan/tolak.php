<?php
    define('PAGE_TITLE', 'Pesanan');
    define('PAGE_LOCATION', 'pesanan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    
    include_once('../../config/controller.php');


    $id_detail_pesanan  = $_GET["id_detail_pesanan"];

    $status="ditolak";
    
    $columns = array(
        'status'=>$status
    );
    
    $condition = "id_detail_pesanan = ".$id_detail_pesanan;
    update('detail_pesanan',$columns, $condition);


    $query="select a.* from pesanan a
            inner join detail_pesanan b on a.kd_pesanan=b.kd_pesanan
            where b.id_detail_pesanan=".$id_detail_pesanan;
    $pesanan = selectDetail($query);

    $query2="select sum(b.harga) as total_bayar from pesanan a
            inner join detail_pesanan b on a.kd_pesanan=b.kd_pesanan
            where b.status='disetujui' and a.kd_pesanan='".$pesanan['kd_pesanan']."'";
    $total_harga = selectDetail($query2);
    $total_bayar=0;
    if($total_harga['total_bayar']!=null){
        $total_bayar=$total_harga['total_bayar'];
    }
    $columns1 = array(
        'total_bayar'=>$total_bayar
    );
    
    $condition1 = "kd_pesanan = '".$pesanan['kd_pesanan']."'";
    update('pesanan',$columns1, $condition1);
    
    echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."pesanan/detail.php?kd_pesanan=".$pesanan['kd_pesanan']."'>";


?>