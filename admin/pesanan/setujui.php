<?php
    define('PAGE_TITLE', 'Pesanan');
    define('PAGE_LOCATION', 'pesanan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');

    session_start(); 
    // If session variable is not set it will redirect to login page
    if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
        header("location:".URL_USER."auth/");
        exit;
    }
    
    include_once('../../config/controller.php');


    $id_detail_pesanan  = $_GET["id_detail_pesanan"];

    $qry="select a.kd_pesanan,a.id_detail_pesanan,a.id_perawatan,
        date(b.tanggal_pesanan)as tanggal_pesan from detail_pesanan a
        inner join pesanan b on a.kd_pesanan = b.kd_pesanan
        where a.id_detail_pesanan=".$id_detail_pesanan;
    $detail_pesanan = selectDetail($qry);

    $qry1="select count(a.id_perawatan) as jumlah_perawatan_disetujui from detail_pesanan a
        inner join pesanan b on a.kd_pesanan=b.kd_pesanan
        where a.status ='disetujui' and b.tanggal_pesanan = date('".$detail_pesanan['tanggal_pesan']."') 
        and a.id_perawatan=".$detail_pesanan['id_perawatan'];
    $jml_perawatan=selectDetail($qry1);

    if($jml_perawatan['jumlah_perawatan_disetujui']>=5){
        echo "<script> alert(\"Pesanan untuk perawatan ini tidak dapat disetujui karena kuota penuh\");</script>";
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."pesanan/detail.php?kd_pesanan=".$detail_pesanan['kd_pesanan']."'>";
    }else{
        $status="disetujui";
        
        $columns = array(
            'status'=>$status
        );
        
        $condition = "id_detail_pesanan = ".$id_detail_pesanan;
        update('detail_pesanan',$columns, $condition);
    
        
        $query2="select sum(b.harga) as total_bayar from pesanan a
                inner join detail_pesanan b on a.kd_pesanan=b.kd_pesanan
                where b.status='disetujui' and b.kd_pesanan='".$detail_pesanan['kd_pesanan']."'";
        $total_harga = selectDetail($query2);
        
        $columns1 = array(
            'total_bayar'=>$total_harga['total_bayar']
        );
        
        $condition1 = "kd_pesanan = '".$detail_pesanan['kd_pesanan']."'";
        update('pesanan',$columns1, $condition1);
        
        echo "<meta http-equiv='refresh' content='0;url=".URL_ADMIN."pesanan/detail.php?kd_pesanan=".$detail_pesanan['kd_pesanan']."'>";
    }
    
?>