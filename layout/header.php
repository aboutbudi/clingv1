<!-- banner -->
<div class="banner page-head">
	<div class="container">
		<div class="header">
			<div class="logo">
				<h3>
					<a class="link link--yaku" href="<?php echo URL_USER ?>">
						<span>C</span><span>L</span><span>I</span><span>N</span><span>G</span>					
					</a>
				</h3>
			</div>
			<div class="navigation">
				<span class="menu"><img src="<?php echo URL_USER ?>images/menu.png" alt=""/></span>
				<ul class="nav1">
					<li><a class="hvr-overline-from-center button2 <?php echo PAGE_LOCATION == 'about' ? 'active':''?>" href="<?php echo URL_USER ?>website/about/">Tentang Kami</a></li>
                    <li><a class="hvr-overline-from-center button2 <?php echo PAGE_LOCATION == 'perawatan' ? 'active':''?>" href="<?php echo URL_USER ?>website/perawatan/">Perawatan</a></li>
                    <li><a class="hvr-overline-from-center button2 <?php echo PAGE_LOCATION == 'member' ? 'active':''?>" href="<?php echo URL_USER ?>website/member/">Membership</a></li>
					 <li><a class="hvr-overline-from-center button2 <?php echo PAGE_LOCATION == 'booking' ? 'active':''?>" href="<?php echo URL_USER ?>website/booking/">Booking</a></li> 
					<li><a class="hvr-overline-from-center button2 <?php echo PAGE_LOCATION == 'kontak' ? 'active':''?>" href="<?php echo URL_USER ?>website/kontak/">Kontak</a></li>
				</ul>
				<!-- script for menu -->
					<script> 
						$( "span.menu" ).click(function() {
						$( "ul.nav1" ).slideToggle( 300, function() {
						 // Animation complete.
						});
						});
					</script>
				<!-- //script for menu -->
			</div>
			<!-- <div class="search">
				<form>
					<input type="search" value="Search" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Search';}" required="">
					<input type="submit" value=" ">
				</form>
			</div>
			<div class="clearfix"></div> -->
        </div>
	</div>
</div>
<!-- //banner -->