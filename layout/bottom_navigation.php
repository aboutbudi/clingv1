<!-- footer -->
	<div class="footer">
		<div class="container">				 	
			<div class="col-md-3 ftr_navi ftr">
				<h3>Navigation</h3>
				<ul>
					<li><a href="<?php echo URL_USER."website/about/"?>">Tentang Kami</a></li>
					<li><a href="<?php echo URL_USER."website/perawatan/"?>">Perawatan</a></li>
					<li><a href="<?php echo URL_USER."website/member/"?>">Membership</a></li>
					<li><a href="<?php echo URL_USER."website/booking/"?>">Booking</a></li>
					<li><a href="<?php echo URL_USER."website/kontak/"?>">Kontak</a></li>
					<li><a href="<?php echo URL_USER."auth/"?>">Login</a></li>
				</ul>
			</div>
			<div class="col-md-3 ftr_navi ftr">
				<h3>Get In Touch</h3>
				<h2>Cling</h2>
				<p>Diamond street,</p>
				<p>260-14 City, Country</p>
				<p>+62 000-0000-00</p>
				<a href="mailto:info@example.com">info@example.com </a>
			</div>
			<!-- <div class="col-md-3 ftr_navi ftr">
				<h3>Subscribe</h3>
				<form>
					<input type="text" value="Enter your email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Enter your email';}" required="">
					<span class="glyphicon glyphicon-envelope" aria-hidden="true"><input type="submit" value=" "></span>
				</form>
			</div> -->
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- //footer -->