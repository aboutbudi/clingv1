<html>
    <head>
        <title><?php echo 'Cling SkinCare | '.PAGE_TITLE;?></title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Cling SkinCare, Cling, Facials, Skin Treatment, Smartphone Compatible web template" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
                function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/font-awesome/css/font-awesome.min.css">
        <!-- //for-mobile-apps -->
        <link href="<?php echo URL_USER ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo URL_USER ?>css/style.css" rel="stylesheet" type="text/css" media="all" />
         <!-- Date Picker -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <!-- Daterange picker -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/bootstrap-daterangepicker/daterangepicker.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/select2/dist/css/select2.min.css">
         <!-- js -->
        <script src="<?php echo URL_USER ?>js/jquery-1.11.1.min.js"></script>
        <!-- //js -->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
            <!-- start-smoth-scrolling -->
                <script type="text/javascript" src="<?php echo URL_USER ?>js/move-top.js"></script>
                <script type="text/javascript" src="<?php echo URL_USER ?>js/easing.js"></script>
                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $(".scroll").click(function(event){		
                            event.preventDefault();
                            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                        });
                    });
                </script>
            <!-- start-smoth-scrolling -->
    </head>
<body>
    <?php
        include_once('header.php');
        include_once($content_page);
        include_once('bottom_navigation.php');
        include_once('footer.php');
    ?>
<!-- for bootstrap working -->
    <!-- jQuery 3 -->
    <script src="<?php echo URL_USER ?>bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?php echo URL_USER ?>bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button);
    </script>
	<script src="<?php echo URL_USER ?>js/bootstrap.js"></script>
    <!-- datepicker -->
    <script src="<?php echo URL_USER ?>bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Select2 -->
    <script src="<?php echo URL_USER ?>bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- //for bootstrap working -->
<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
    <script>
        $(function () {
            //Date picker
            $('#datepicker').datepicker({
            autoclose: true
            })
            //Timepicker
            $('.timepicker').timepicker({
            showInputs: false
            })
        })
    </script>
</body>
</html>