<?php
    define('PAGE_TITLE', 'Perawatan');
    define('PAGE_LOCATION', 'perawatan');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    
    include_once('../../config/controller.php');

    $perawatan = array();
    $query = "SELECT * FROM perawatan LIMIT 9";
    $perawatan =  selectBySql($query);

    $content_page='../template/index_perawatan.php';

    include_once('../../layout/main_layout.php');
?>