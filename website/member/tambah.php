<?php
    define('PAGE_TITLE', 'Member');
    define('PAGE_LOCATION', 'member');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    include_once('../../config/link.php');
    include_once('../../config/controller.php');

    // $content_page='../template/form_member.php';

    // include_once('../../layout/main_layout.php');

    // Define variables and initialize with empty values
    $email = $nama_pelanggan = $nomor_telepon = $alamat = "";
    $email_err = "";

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        
        if(empty(trim($_POST["email"]))){
            $email_err = "Please enter a email.";
        } else{
            // Prepare a select statement
            $sql = "SELECT nomor_pelanggan FROM pelanggan where email= ?";
            
            if($stmt = mysqli_prepare($link, $sql)){
                // Bind variables to the prepared statement as parameters
                mysqli_stmt_bind_param($stmt, "s", $param_email);
                
                // Set parameters
                $param_email = trim($_POST["email"]);
                
                // Attempt to execute the prepared statement
                if(mysqli_stmt_execute($stmt)){
                    /* store result */
                    mysqli_stmt_store_result($stmt);
                    
                    if(mysqli_stmt_num_rows($stmt) == 1){
                        $email_err = "This email is already taken.";
                        $email = trim($_POST["email"]);
                        $nama_pelanggan = trim($_POST["nama_pelanggan"]);
                        $nomor_telepon = trim($_POST["nomor_telepon"]);
                        $alamat = trim($_POST["alamat"]);
                    } else{
                        $email = trim($_POST["email"]);
                    }
                } else{
                    echo "Oops! Something went wrong. Please try again later.";
                }
            }
            
            // Close statement
            mysqli_stmt_close($stmt);
        }
        
        //Check input errors before inserting in database
        if(empty($email_err)){
            // Prepare an insert statement
            $nama_pelanggan = isset($_POST['nama_pelanggan']) ? $_POST['nama_pelanggan'] : "";
            $nomor_telepon = isset($_POST['nomor_telepon']) ? $_POST['nomor_telepon'] : "";
            $alamat = isset($_POST['alamat']) ? $_POST['alamat'] : "";
            
            $query_last_member ="SELECT * FROM pelanggan order by nomor_pelanggan desc limit 1";
            $hasil_query_last_member=selectDetail($query_last_member);

            $_last_record=substr($hasil_query_last_member['nomor_pelanggan'], -1);
            $last_record=(int)$_last_record;

            $no_reg="";
            if($last_record<10){
                $no_reg= date("dmy")."000".$last_record+1;
            }elseif($last_record>=10&&$last_record<100){
                $no_reg= date("dmy")."00".$last_record+1;
            }elseif($last_record>=100){
                $no_reg= date("dmy")."0".$last_record+1;
            }

            $nomor="MBR".$no_reg;
            $foto="";

            $nama_file = $_FILES['foto']['name'];
            $ukuran_file = $_FILES['foto']['size'];
            $tipe_file = $_FILES['foto']['type'];
            $tmp_file = $_FILES['foto']['tmp_name'];

            $uploaddir = '../../upload/';
            if($nama_file==""){
                $values = array($nama_pelanggan,$email,$nomor_telepon,$alamat,$nomor);
                $columns = array('nama_pelanggan', 'email', 'nomor_telepon','alamat','nomor_pelanggan');
                
                insert('pelanggan', $values, $columns);
                echo "<meta http-equiv='refresh' content='0;url=".URL_USER."website/member/success.php?email=".$email."'>";
            }else{
                $foto = 'pelanggan-'.$nama_file;
                $uploadfile = $uploaddir . $foto;
                if($tipe_file == "image/jpg" ||$tipe_file == "image/jpeg" || $tipe_file == "image/png"){
                    if($ukuran_file <= 1000000){
                        if(move_uploaded_file($tmp_file, $uploadfile)){
                            $values = array($nama_pelanggan,$email,$nomor_telepon,$alamat,$nomor,$foto);
                            $columns = array('nama_pelanggan', 'email', 'nomor_telepon','alamat','nomor_pelanggan','foto');
                            
                            insert('pelanggan', $values, $columns);
                            echo "<meta http-equiv='refresh' content='0;url=".URL_USER."website/member/success.php?email=".$email."'>";
                        }else{
                            echo "<script>alert(\"File gagal diupload\");</script>";
                        }
                    }else{
                        echo "<script>alert(\"Ukuran file terlalu besar\");</script>";
                    }
                }else{
                    echo "<script>alert(\"File yang anda masukan bukan gambar\");</script>";
                }
            }
        }
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <title><?php echo 'Cling SkinCare | '.PAGE_TITLE;?></title>
        <!-- for-mobile-apps -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="keywords" content="Cling SkinCare, Cling, Facials, Skin Treatment, Smartphone Compatible web template" />
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
                function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo URL_USER ?>bower_components/font-awesome/css/font-awesome.min.css">
        <!-- //for-mobile-apps -->
        <link href="<?php echo URL_USER ?>css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="<?php echo URL_USER ?>css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- js -->
        <script src="<?php echo URL_USER ?>js/jquery-1.11.1.min.js"></script>
        <!-- //js -->
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
            <!-- start-smoth-scrolling -->
                <script type="text/javascript" src="<?php echo URL_USER ?>js/move-top.js"></script>
                <script type="text/javascript" src="<?php echo URL_USER ?>js/easing.js"></script>
                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $(".scroll").click(function(event){		
                            event.preventDefault();
                            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
                        });
                    });
                </script>
            <!-- start-smoth-scrolling -->
    </head>
<body>
    <?php
        include_once('../../layout/header.php');
        ?>
        <style>
        .image-preview-input {
            position: relative;
            overflow: hidden;
            margin: 0px;    
            color: #333;
            background-color: #fff;
            border-color: #ccc;    
        }
        .image-preview-input input[type=file] {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            padding: 0;
            font-size: 20px;
            cursor: pointer;
            opacity: 0;
            filter: alpha(opacity=0);
        }
        .image-preview-input-title {
            margin-left:2px;
        }
        </style>
        <div class="about">
            <div class="container">
                <h3 class="tittle-one">Pendaftaran Member</h3>
            </div>
            <div class="row">
                <div class="col-md-2">
                </div>
                <div class="col-md-8">
                    <h5><i class="fa fa-warning"></i> Silahkan masukkan data diri dengan benar !</h5>
                    <form enctype="multipart/form-data" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" class="form-horizontal">
                        <div class="form-group">
                            <label for="nama_pelanggan" class="col-sm-3 control-label">Nama Lengkap</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" value="<?php echo $nama_pelanggan; ?>" required>
                            </div>
                        </div>
                        <div class="form-group <?php echo (!empty($email_err)) ? 'has-error' : ''; ?>">
                            <label for="email" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" class="form-control" id="email" name="email" value="<?php echo $email; ?>" required>
                                <span class="help-block"><?php echo $email_err; ?></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nomor_telepon" class="col-sm-3 control-label">Nomor Telepon</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="nomor_telepon" name="nomor_telepon" value="<?php echo $nomor_telepon; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="alamat" class="col-sm-3 control-label">Alamat</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="alamat" name="alamat" value="<?php echo $alamat; ?>" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="foto" class="col-sm-3 control-label">Foto</label>
                            <div class="col-sm-9">
                                <div class="input-group image-preview">
                                    <input type="text" class="form-control image-preview-filename" disabled="disabled"> <!-- don't give a name === doesn't send on POST/GET -->
                                    <span class="input-group-btn">
                                        <!-- image-preview-clear button -->
                                        <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                                            <span class="glyphicon glyphicon-remove"></span> Clear
                                        </button>
                                        <!-- image-preview-input -->
                                        <div class="btn btn-default image-preview-input">
                                            <span class="glyphicon glyphicon-folder-open"></span>
                                            <span class="image-preview-input-title">Browse</span>
                                            <input type="file" accept="image/png, image/jpeg, image/gif" name="foto"/> <!-- rename it -->
                                        </div>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                            </div>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-success">Sign Up</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-2">
                </div>
            </div>
        </div>
        <script>
            $(document).on('click', '#close-preview', function(){ 
            $('.image-preview').popover('hide');
            // Hover befor close the preview
            $('.image-preview').hover(
                function () {
                $('.image-preview').popover('show');
                }, 
                function () {
                $('.image-preview').popover('hide');
                }
            );    
        });

        $(function() {
            // Create the close button
            var closebtn = $('<button/>', {
                type:"button",
                text: 'x',
                id: 'close-preview',
                style: 'font-size: initial;',
            });
            closebtn.attr("class","close pull-right");
            // Set the popover default content
            $('.image-preview').popover({
                trigger:'manual',
                html:true,
                title: "<strong>Preview</strong>"+$(closebtn)[0].outerHTML,
                content: "There's no image",
                placement:'top'
            });
            // Clear event
            $('.image-preview-clear').click(function(){
                $('.image-preview').attr("data-content","").popover('hide');
                $('.image-preview-filename').val("");
                $('.image-preview-clear').hide();
                $('.image-preview-input input:file').val("");
                $(".image-preview-input-title").text("Browse"); 
            }); 
            // Create the preview image
            $(".image-preview-input input:file").change(function (){     
                var img = $('<img/>', {
                    id: 'dynamic',
                    width:250,
                    height:200
                });      
                var file = this.files[0];
                var reader = new FileReader();
                // Set preview image into the popover data-content
                reader.onload = function (e) {
                    $(".image-preview-input-title").text("Change");
                    $(".image-preview-clear").show();
                    $(".image-preview-filename").val(file.name);            
                    img.attr('src', e.target.result);
                    $(".image-preview").attr("data-content",$(img)[0].outerHTML).popover("show");
                }        
                reader.readAsDataURL(file);
            });  
        });
        </script>
        <?php
        include_once('../../layout/bottom_navigation.php');
        include_once('../../layout/footer.php');
    ?>
<!-- for bootstrap working -->
	<script src="<?php echo URL_USER ?>js/bootstrap.js"></script>
<!-- //for bootstrap working -->
<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear' 
			};
		*/								
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
</body>
</html>