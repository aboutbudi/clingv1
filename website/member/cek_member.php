<?php
    define('PAGE_TITLE', 'Member');
    define('PAGE_LOCATION', 'member');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    include_once('../../config/link.php');
    include_once('../../config/controller.php');

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $email = trim($_POST["email"]);

        // Prepare a select statement
        $sql = "SELECT nomor_pelanggan FROM pelanggan where email= ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_email);
            
            // Set parameters
            $param_email = trim($_POST["email"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) >= 1){
                    mysqli_stmt_bind_result($stmt,$nomor_pelanggan);
                    if(mysqli_stmt_fetch($stmt)){
                        session_start();
                        $_SESSION['nomor_pelanggan'] = $nomor_pelanggan;
                        header("location: ".URL_USER."website/member/detail_member.php");
                    }
                }else{
                    echo "<meta http-equiv='refresh' content='0;url=".URL_USER."website/member/error.php?email=".$email."'>";
                }
            }
        }
        mysqli_stmt_close($stmt);
    }
?>