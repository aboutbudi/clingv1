<?php
    define('PAGE_TITLE', 'Booking');
    define('PAGE_LOCATION', 'booking');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    // Initialize the session
    session_start();
    include_once('../../config/controller.php');
    
    $nomor_pelanggan = $_SESSION['nomor_pelanggan'];
    $query = "SELECT * FROM pelanggan WHERE nomor_pelanggan= '".$nomor_pelanggan."'";
    $pelanggan =  selectDetail($query);
    
    $nomor_pesanan = $_SESSION['nomor_pesanan'];
    $query2 = "SELECT *,DATE_FORMAT(tanggal_pesanan, \"%e %M %Y\") as tanggal_pesan FROM pesanan WHERE kd_pesanan= '".$nomor_pesanan."'";
    $pesanan =  selectDetail($query2);
    
    $perawatan = array();
    $query = "SELECT * FROM perawatan";
    $perawatan =  selectBySql($query);

    $detail_pesanan = array();
    $query3 = "SELECT * FROM detail_pesanan a LEFT JOIN perawatan b on a.id_perawatan = b.id_perawatan WHERE kd_pesanan='".$nomor_pesanan."'";
    $detail_pesanan =  selectBySql($query3);

    $query4="SELECT COUNT(id_detail_pesanan) as jumlah_detail_pesanan FROM detail_pesanan WHERE kd_pesanan='".$nomor_pesanan."'";
    $jumlah_detail_pesanan =  selectDetail($query4);

    $query5="select a.kd_pesanan, sum(a.harga) as total_bayar_seharusnya, 
            b.total_bayar as total_bayar_belum_disetujui,
            c.total_bayar as total_bayar_disetujui,
            d.total_bayar as total_bayar_ditolak 
            from detail_pesanan a
            left join (select kd_pesanan, sum(harga) as total_bayar from detail_pesanan where (status='belum-submit' or status='belum-disetujui') and kd_pesanan='".$nomor_pesanan."') b on a.kd_pesanan=b.kd_pesanan
            left join (select kd_pesanan, sum(harga) as total_bayar from detail_pesanan where status='disetujui' and kd_pesanan='".$nomor_pesanan."') c on a.kd_pesanan=c.kd_pesanan
            left join (select kd_pesanan, sum(harga) as total_bayar from detail_pesanan where status='ditolak' and kd_pesanan='".$nomor_pesanan."') d on a.kd_pesanan=d.kd_pesanan
            where a.kd_pesanan='".$nomor_pesanan."'";
    $biaya = selectDetail($query5);

    $content_page='../template/form_booking.php';

    include_once('../../layout/main_layout.php');
?>