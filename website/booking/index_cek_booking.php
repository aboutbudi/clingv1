<?php
    define('PAGE_TITLE', 'Booking');
    define('PAGE_LOCATION', 'booking');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    // Initialize the session
    session_start();
    include_once('../../config/controller.php');

    $kd_pesanan = $_SESSION['kd_pesanan'];
    $query = "SELECT *,DATE_FORMAT(a.tanggal_pesanan, \"%e %M %Y\") as tanggal_pesan FROM pesanan a 
                LEFT JOIN pelanggan b ON a.nomor_pelanggan = b.nomor_pelanggan WHERE a.kd_pesanan='".$kd_pesanan."'";
    $pesanan =  selectDetail($query);

    $detail_pesanan = array();
    $query2 = "SELECT * FROM detail_pesanan a LEFT JOIN perawatan b on a.id_perawatan = b.id_perawatan WHERE kd_pesanan='".$kd_pesanan."'";
    $detail_pesanan =  selectBySql($query2);

    $query5="select a.kd_pesanan, sum(a.harga) as total_bayar_seharusnya, 
        b.total_bayar as total_bayar_belum_disetujui,
        c.total_bayar as total_bayar_disetujui,
        d.total_bayar as total_bayar_ditolak 
        from detail_pesanan a
        left join (select kd_pesanan, sum(harga) as total_bayar from detail_pesanan where status='belum-disetujui' and kd_pesanan='".$kd_pesanan."') b on a.kd_pesanan=b.kd_pesanan
        left join (select kd_pesanan, sum(harga) as total_bayar from detail_pesanan where status='disetujui' and kd_pesanan='".$kd_pesanan."') c on a.kd_pesanan=c.kd_pesanan
        left join (select kd_pesanan, sum(harga) as total_bayar from detail_pesanan where status='ditolak' and kd_pesanan='".$kd_pesanan."') d on a.kd_pesanan=d.kd_pesanan
        where a.kd_pesanan='".$kd_pesanan."'";
    $biaya = selectDetail($query5);

    $content_page='../template/cek_booking.php';

    include_once('../../layout/main_layout.php');
?>