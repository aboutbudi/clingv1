<?php
    define('PAGE_TITLE', 'Booking');
    define('PAGE_LOCATION', 'booking');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    
    include_once('../../config/controller.php');

    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $nomor_pelanggan = isset($_GET['nomor_pelanggan']) ? $_GET['nomor_pelanggan'] : "";
        $kd_pesanan = isset($_GET['kd_pesanan']) ? $_GET['kd_pesanan'] : "";

        $error_msg="";
        if($nomor_pelanggan!="" && $kd_pesanan==""){
            $error_msg = "Mohon maaf nomor pelanggan : <b>".$nomor_pelanggan."</b> tidak terdaftar dalam sistem kami";
        }elseif($nomor_pelanggan=="" && $kd_pesanan!=""){
            $error_msg = "Mohon maaf kode booking : <b>".$kd_pesanan."</b> tidak terdaftar dalam sistem kami atau mungkin telah kadaluarsa";
        }

    }

    $content_page='../template/index_error.php';

    include_once('../../layout/main_layout.php');
?>