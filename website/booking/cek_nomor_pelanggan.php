<?php
    define('PAGE_TITLE', 'Booking');
    define('PAGE_LOCATION', 'booking');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    include_once('../../config/link.php');
    include_once('../../config/controller.php');

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $nomor_pelanggan = trim($_POST["nomor_pelanggan"]);

        // Prepare a select statement
        $sql = "SELECT nomor_pelanggan,email FROM pelanggan where nomor_pelanggan= ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_nomor_pelanggan);
            
            // Set parameters
            $param_nomor_pelanggan = trim($_POST["nomor_pelanggan"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) >= 1){
                    mysqli_stmt_bind_result($stmt, $nomor_pelanggan, $email);
                    if(mysqli_stmt_fetch($stmt)){

                        $query_last_booking ="SELECT * FROM pesanan order by tanggal_dibuat desc limit 1";
                        $hasil_query_last_booking=selectDetail($query_last_booking);
                        
                        $_last_record=substr($hasil_query_last_booking['kd_pesanan'], -1);
                        $last_record=(int)$_last_record;
                        
                        $no_reg="";
                        if($last_record<10){
                            $no_reg= date("dmy")."000".$last_record+1;
                        }elseif($last_record>=10&&$last_record<100){
                            $no_reg= date("dmy")."00".$last_record+1;
                        }elseif($last_record>=100){
                            $no_reg= date("dmy")."0".$last_record+1;
                        }
                        $nomor="PSN".$no_reg;
                        $temp_tanggal_pesanan=trim($_POST["tanggal_pesanan"]);
                        $tem_tanggal_pesanan = strtotime($temp_tanggal_pesanan);
                        $tanggal_pesanan = date('Y-m-d',$tem_tanggal_pesanan);

                        $values = array($nomor_pelanggan,$tanggal_pesanan,$nomor);
                        $columns = array('nomor_pelanggan', 'tanggal_pesanan', 'kd_pesanan');
                        
                        insert('pesanan', $values, $columns);

                        session_start();
                        $_SESSION['nomor_pelanggan'] = $nomor_pelanggan;
                        $_SESSION['nomor_pesanan'] = $nomor;  
                        header("location: ".URL_USER."website/booking/new_booking.php");
                    }
                }else{
                    echo "<meta http-equiv='refresh' content='0;url=".URL_USER."website/booking/error.php?nomor_pelanggan=".$nomor_pelanggan."'>";
                }
            }
        }
        mysqli_stmt_close($stmt);
        
    }

    // $content_page='../template/index_booking.php';

    // include_once('../../layout/main_layout.php');
?>