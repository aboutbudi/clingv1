<?php
    define('PAGE_TITLE', 'Booking');
    define('PAGE_LOCATION', 'booking');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    include_once('../../config/link.php');
    include_once('../../config/controller.php');

    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $kd_pesanan = trim($_POST["kd_pesanan"]);

        // Prepare a select statement
        $sql = "SELECT kd_pesanan FROM pesanan where kd_pesanan= ?";
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_kd_pesanan);
            
            // Set parameters
            $param_kd_pesanan = trim($_POST["kd_pesanan"]);
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt)){
                /* store result */
                mysqli_stmt_store_result($stmt);
                
                if(mysqli_stmt_num_rows($stmt) >= 1){
                    mysqli_stmt_bind_result($stmt, $kd_pesanan);
                    if(mysqli_stmt_fetch($stmt)){
                        session_start();
                        $_SESSION['kd_pesanan'] = $kd_pesanan;  
                        header("location: ".URL_USER."website/booking/index_cek_booking.php");
                    }
                }else{
                    echo "<meta http-equiv='refresh' content='0;url=".URL_USER."website/booking/error.php?kd_pesanan=".$kd_pesanan."'>";
                }
            }
        }
        mysqli_stmt_close($stmt);
        
    }

    $content_page='../template/index_booking.php';

    include_once('../../layout/main_layout.php');
?>