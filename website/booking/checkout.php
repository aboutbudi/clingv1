<?php
    define('PAGE_TITLE', 'Booking');
    define('PAGE_LOCATION', 'booking');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    // Initialize the session
    session_start();
    include_once('../../config/controller.php');
    
    $nomor_pelanggan = $_SESSION['nomor_pelanggan'];
    $query = "SELECT * FROM pelanggan WHERE nomor_pelanggan= '".$nomor_pelanggan."'";
    $pelanggan =  selectDetail($query);
    
    $nomor_pesanan = $_SESSION['nomor_pesanan'];
    $query2 = "SELECT *,DATE_FORMAT(tanggal_pesanan, \"%e %M %Y\") as tanggal_pesan FROM pesanan WHERE kd_pesanan= '".$nomor_pesanan."'";
    $pesanan =  selectDetail($query2);
    
    if($_SERVER['REQUEST_METHOD'] == 'GET'){
        $kd_pesanan = isset($_GET['kd_pesanan']) ? $_GET['kd_pesanan'] : "";
        
        $status="belum-disetujui";

        $columns1 = array(
            'status'=>$status
        );
        
        $condition = "kd_pesanan = '".$kd_pesanan."'";
        update('detail_pesanan',$columns1, $condition);

        // Initialize the session
        session_start();
        
        // Unset all of the session variables
        $_SESSION = array();
        
        // Destroy the session.
        session_destroy();
        
        // Redirect to login page
        header("location: ".URL_USER."website/booking");
        exit;
    }
?>