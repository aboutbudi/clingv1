<?php
    define('PAGE_TITLE', 'Booking');
    define('PAGE_LOCATION', 'booking');
    define('URL_USER', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/');
    define('URL_ADMIN', 'http://'.$_SERVER['HTTP_HOST'].'/clingv1/admin/');
    // Initialize the session
    session_start();
    include_once('../../config/controller.php');
    
    $nomor_pelanggan = $_SESSION['nomor_pelanggan'];
    $query = "SELECT * FROM pelanggan WHERE nomor_pelanggan= '".$nomor_pelanggan."'";
    $pelanggan =  selectDetail($query);
    
    $nomor_pesanan = $_SESSION['nomor_pesanan'];
    $query2 = "SELECT *,DATE_FORMAT(tanggal_pesanan, \"%e %M %Y\") as tanggal_pesan FROM pesanan WHERE kd_pesanan= '".$nomor_pesanan."'";
    $pesanan =  selectDetail($query2);
    
    if($_SERVER['REQUEST_METHOD'] == 'POST'){
        $id_perawatan = isset($_POST['id_perawatan']) ? $_POST['id_perawatan'] : "";
        
        $query3 = "SELECT * FROM perawatan WHERE id_perawatan=".$id_perawatan;
        $perawatan =  selectDetail($query3);
        $status="belum-submit";

        $values = array($nomor_pesanan,$perawatan['id_perawatan'],$perawatan['harga'],$status);
        $columns = array('kd_pesanan','id_perawatan','harga','status');        
        insert('detail_pesanan', $values, $columns);

        $detail_pesanan = array();
        $query4 = "SELECT SUM(harga) as total_harga FROM detail_pesanan WHERE kd_pesanan='".$nomor_pesanan."'";
        $detail_pesanan =  selectDetail($query4);
        // $columns1 = array(
        //     'total_bayar'=>$detail_pesanan['total_harga']
        // );
        
        // $condition = "id_pesanan = ".$pesanan['id_pesanan'];
        // update('pesanan',$columns1, $condition);

        echo "<meta http-equiv='refresh' content='0;url=".URL_USER."website/booking/new_booking.php'>";
    }
?>