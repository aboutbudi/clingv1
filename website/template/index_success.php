<div class="about">
    <div class="container">
        <h3 class="tittle-one">Pendaftaran Member</h3>
    </div>
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <h5><i class="fa fa-check"></i> Selamat anda sudah resmi menjadi member Cling SkinCare !</h5>
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="form form-horizontal">
                            <div class="form-group">
                                <label for="nama_pelanggan" class="col-sm-4 control-label">Nama Lengkap</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" value="<?php echo $pelanggan['nama_pelanggan']; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nomor_pelanggan" class="col-sm-4 control-label">Nomor Pelanggan</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nomor_pelanggan" name="nomor_pelanggan" value="<?php echo $pelanggan['nomor_pelanggan']; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_pelanggan" class="col-sm-4 control-label">Email</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" value="<?php echo $pelanggan['email']; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_pelanggan" class="col-sm-4 control-label">Nomor Telepon</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" value="<?php echo $pelanggan['nomor_telepon']; ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="nama_pelanggan" class="col-sm-4 control-label">Alamat</label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="nama_pelanggan" name="nama_pelanggan" value="<?php echo $pelanggan['alamat']; ?>" readonly>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <img src="<?php echo URL_USER."upload/".$pelanggan['foto']; ?>" class="img thumbnail" style="max-width:100%;" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <a href="<?php echo URL_USER."website/booking"?>" class="btn btn-success pull-right"><i class="fa fa-shower"></i> Pesan Perawatan</a>
                </div>
            </div>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>