<div class="tips" style="background:#fff;">
	<div class="container">
		<h3 class="tittle-one">Perawatan</h3>
		<div class="tip-grids">
            <?php 
                if (is_array($perawatan) || is_object($perawatan)){
                    foreach($perawatan as $perawatan)
                    {
            ?>
            <div class="col-md-4 tip-grid" style="margin:20px 0 20px 0;">
				<figure class="effect-julia">
                    <a href="<?php echo URL_USER."website/perawatan/detail.php?id_perawatan=".$perawatan['id_perawatan'];?>">
					<img src="<?php echo $perawatan['foto'] ? URL_USER."upload/".$perawatan['foto'] : URL_USER."images/ban2.jpg" ?>" alt="<?php echo $perawatan['nama_perawatan'] ?>" />
						<figcaption>
							<h4 style="font-size:25px;"><?php echo $perawatan['nama_perawatan'] ?></h4>
								<p><?php echo "Rp. ".$perawatan['harga'] ?></p>
                        </figcaption>
                    </a>			
				</figure>
            </div>
            <?php
                    }
                }
            ?>
			
			<div class="clearfix"></div>
		</div>
	</div>
</div>
