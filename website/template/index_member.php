<!-- features -->
<div class="feature">
	<div class="container">
		<h3 class="tittle-one">Membership</h3>
		<div class="feature-grids">
			<!--<div class="col-md-3 feature-grid text-center">
				
			</div>-->
			<div class="col-md-6 feature-grid text-center">
				<div class="feature-grid-one">
					<a href="<?php echo URL_USER."website/member/tambah.php"?>" class="hi-icon hi-icon-archive fa fa-user-plus"> </a>
					<h4>Daftar Member</h4>
					<p>Belum bergabung sebagai member, daftar untuk menikmati sejuta fasilitas member di Cling SkinCare</p>
				</div>
				
			</div>
			<div class="col-md-6 feature-grid text-center">
				<div class="feature-grid-two">
					<a href="#"data-toggle="modal" data-target="#cek-pelanggan" class="hi-icon hi-icon-archive fa fa-user"> </a>
					<h4>Cek Member</h4>
					<p>Sudah Bergabung tapi lupa detail data anda, cek data kamu di sini untuk melakukan transaksi di Cling SkinCare</p>
				</div>
			</div>
			<!--
			<div class="col-md-3 feature-grid text-center">
				
			</div>-->
			<div class="clearfix"></div>
        </div>
        
	</div>
</div>

<div class="modal fade" id="cek-pelanggan">
        <div class="modal-dialog">
        <div class="modal-content">
             <form action="<?php echo URL_USER."website/member/cek_member.php" ?>" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cek Data Pelanggan</h4>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input type="email" class="form-control" id="email" name="email" Requaried>
                        </div>
                    </div>
                    <br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Cek</button>
                </div>
            </form> 
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>