<!-- about -->
	<!--start-about-->
	<div class="about">
		<div class="container">
			<div class="about-top heading">
				<h3 class="tittle-one">Tentang Kami</h3>
			</div>
			<div class="about-bottom l-grids text-center">
				<figure class="effect-bubba">
					<img src="<?php echo URL_USER?>images/ban2.jpg" alt=""/>
					<figcaption>
						<h4>Cling</h4>
						<p>Kecentikan tidak harus dari wajah tetapi juga dari hati.</p>																
					</figcaption>			
				</figure>
			</div>
			<!-- <h5>Sed hendrerit dui eget lacus imperdiet lacinia. Sed eleifend,
				libero ac pellentesque ornare, velit velit tempor nisi, quis cursus dolor velit condimentum elit.</h5> -->
			<h5></h5>
			<p>Produk perawatan dari klinik kecantikan Cling yang dikenal umum adalah <em>facial</em> . Perawatan <em>facial</em> adalah sebuah prosedur yang melibatkan berbagai perawatan kulit, termasuk: penguapan, pengelupasan, ekstraksi, krim, <em>lotion</em>, pengunaan masker, dan pemijatan. Biasanya dilakukan di salon kecantikan tetapi juga dapat ditemukan di berbagai perawatan spa.</p>
		</div>
	</div>	
<!--//about-->