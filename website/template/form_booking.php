<div class="about">
	<div class="container">
		<h5 class="tittle-one">Booking Perawatan</h5>
		<div class="row">
			<div class="col-md-3">
			</div>
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-responsive">
					<thead>
						<tr>
							<th colspan=2 style="background:grey;text-align:center;color:#fff;"><h4>Data Pesanan</h4></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Naama Pelanggan</td>
							<td><?php echo strtoupper($pelanggan['nama_pelanggan'])." - ".$pelanggan['nomor_pelanggan'];?></td>
						</tr>
						<tr>
							<td>Kode Booking</td>
							<td><?php echo $pesanan['kd_pesanan'];?></td>
						</tr>
						<tr>
							<td>Tanggal Booking</td>
							<td><?php echo $pesanan['tanggal_pesan'];?></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-3">
			</div>
		</div>
		<h5>Silahkan melakukan pemesanan untuk perawatan di Cling SkinCare</h5>
		<p><em>*Pemesanan dapat dilakukan paling lambat <b>2 jam</b> sebelum perawatan</em></p>
		<div class="row">
			<h5><a href="#" data-toggle="modal" data-target="#tambah-perawatan" class="btn btn-success">Tambah Perawatan</a></h5>
			<div class="col-xs-12">
				<table class="table table-bordered table-striped table-responsive">
					<thead>
						<tr>
							<th style="text-align:center">Perawatan</th>
							<th style="text-align:center">Harga</th>
							<th style="text-align:center">Status</th>
							<th style="text-align:center">Option</th>
						</tr>
					</thead>
					<tbody>
					<?php 
						if (is_array($detail_pesanan) || is_object($detail_pesanan)){
						foreach($detail_pesanan as $detail_pesanan)
						{
					?>
						<tr>
							<td><?php echo $detail_pesanan['nama_perawatan'];?></td>
							<td style="text-align:right;width:200px;"><?php echo "Rp. ".$detail_pesanan['harga'];?></td>
							<td style="width:200px;"><?php echo $detail_pesanan['status'];?></td>
							<td style="width:100px;"><a href="<?php echo URL_USER."website/booking/hapus.php?id_detail_pesanan=".$detail_pesanan['id_detail_pesanan'];?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Hapus</a></td>
						</tr>
					<?php 
						}
						}
					?>
					</tbody>
					<tfoot>
						<tr>
							<th style="text-align:right;font-weight:bold;">
								Total Belum Disetujui </th>
							<th style="text-align:right;font-weight:bold;width :120px;">
								<?php echo $biaya['total_bayar_belum_disetujui'] ? "Rp. ".$biaya['total_bayar_belum_disetujui'].",00" : "-" ;?>
							</th>
						</tr>
						<tr>
							<th style="text-align:right;font-weight:bold;">
								Total Disetujui</th>
							<th style="text-align:right;font-weight:bold;width :190px;">
								<?php echo $biaya['total_bayar_disetujui'] ? "Rp. ".$biaya['total_bayar_disetujui'].",00" : "-" ;?>
							</th>
						</tr>
						
						<tr>
							<th style="text-align:right;font-weight:bold;">Total Ditolak</th>
							<th style="text-align:right;font-weight:bold;width :190px;">
								<?php echo $biaya['total_bayar_ditolak'] ? "Rp. ".$biaya['total_bayar_ditolak'].",00" : "-" ;?>
							</th>
						</tr>
						<tr>
							<th style="text-align:right;font-weight:bold;">Estimasi Total Bayar</th>
							<th style="text-align:right;font-weight:bold;width :190px;">
								<?php echo $biaya['total_bayar_seharusnya'] ? "Rp. ".$biaya['total_bayar_seharusnya'].",00" : "-" ;?>
							</th>
						</tr>
					</tfoot>
				</table>
			</div>
			
		</div>
		<div class="row">
			<div class="col-xs-12" style="text-align:right;">
				<a href="#" data-toggle="modal" data-target="#batal-pesan" class="btn btn-danger">Batalkan Pesanan</a>
				<?php if($jumlah_detail_pesanan['jumlah_detail_pesanan'] > 0){?>
					<a href="<?php echo URL_USER."website/booking/checkout.php?kd_pesanan=".$pesanan['kd_pesanan']; ?>" class="btn btn-success">Submit Pesanan</a>
				<?php }?>
				
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="tambah-perawatan">
    <div class="modal-dialog">
        <div class="modal-content">
                <form action="<?php echo URL_USER."website/booking/tambah.php" ?>" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Perawatan</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label for="nama_perawatan" class="col-md-4 control-label">Pilih Perawatan</label>
                            <div class="col-md-8">
                                <select class="form-control" name="id_perawatan">
									<?php
										foreach($perawatan as $perawatan){
									?>
									<option value="<?php echo $perawatan['id_perawatan']; ?>"><?php echo $perawatan['nama_perawatan']." - Rp. ".$perawatan['harga']; ?></option>
									<?php
										}
									?>
								</select>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Tambah</button>
                </div>
            </form> 
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="batal-pesan">
    <div class="modal-dialog">
        <div class="modal-content">
                <form action="<?php echo URL_USER."website/booking/batal_pesan.php" ?>" method="post">
				<input type="hidden" name="kd_pesanan" value="<?php echo $pesanan['kd_pesanan']?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Batal Pesan</h4>
                </div>
                <div class="modal-body">
					<p>Apakah anda yakin ingin membatalkan pesanan ini :</p>
					<br>
                    <table class="table table-responsive table-bordered table-striped">
						<tbody>
							<tr>
								<td>Kode Pesan</td>
								<td><b><?php echo $pesanan['kd_pesanan'];?></b></td>
							</tr>
							<tr>
								<td>Nama Pelanggan</td>
								<td><b><?php echo strtoupper($pelanggan['nama_pelanggan'])." - ".$pelanggan['nomor_pelanggan'];?></b></td>
							</tr>
							<tr>
								<td>Tanggal Pesan</td>
								<td><b><?php echo $pesanan['tanggal_pesan'];?></b></td>
							</tr>
						</tbody>
					</table>
					<p><small><em><b>Jika anda sudah memilih untuk membatalkan pesanan ini maka tidak dapat di roleback kembali dan kode pesanan sudah tidak berlaku</b></em></small></p>
					<br><br>
                </div>
                <div class="modal-footer">
					<button type="button" class="btn btn-warning" data-dismiss="modal" aria-label="Close">Tidak</button>
					<button type="submit" class="btn btn-danger"><i class="fa fa-trash-o"></i> Batalkan Pesanan</button>
                </div>
            </form> 
        </div>
    <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>	