<div class="about">
	<div class="container">
		<h5 class="tittle-one">Cek Data Pelanggan</h5>
		<div class="row">
			<div class="col-md-3">
			</div>
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-responsive">
					<thead>
						<tr>
							<th colspan=2 style="background:grey;text-align:center;color:#fff;"><h4>Detail Data Pelanggan</h4></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Nama Pelanggan</td>
							<td><?php echo $detail_pelanggan['nama_pelanggan']; ?></td>
						</tr>
						<tr>
							<td>Nomor Pelanggan</td>
							<td><?php echo $detail_pelanggan['nomor_pelanggan']; ?></td>
						</tr>
						<tr>
							<td>Email</td>
							<td><?php echo $detail_pelanggan['email']; ?></td>
						</tr>
						<tr>
							<td>No Telepon</td>
							<td><?php echo $detail_pelanggan['nomor_telepon']; ?></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td><?php echo $detail_pelanggan['alamat']; ?></td>
						</tr>
					</tbody>
				</table>
				<a href="<?php echo URL_USER."website/member"?>" class="btn btn-success pull-right">Kembali</a>
			</div>
			<div class="col-md-3">
			</div>
		</div>
	</div>
</div>