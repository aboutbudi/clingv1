<div class="about">
	<div class="container">
		<h5 class="tittle-one">Detail Pemesanan</h5>
		<div class="row">
			<div class="col-md-3">
			</div>
			<div class="col-md-6">
				<table class="table table-bordered table-striped table-responsive">
					<thead>
						<tr>
							<th colspan=2 style="background:grey;text-align:center;color:#fff;"><h4>Data Pesanan</h4></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>Naama Pelanggan</td>
							<td><?php echo strtoupper($pesanan['nama_pelanggan'])." - ".$pesanan['nomor_pelanggan'] ;?></td>
						</tr>
						<tr>
							<td>Kode Booking</td>
							<td><?php echo $pesanan['kd_pesanan'];?></td>
						</tr>
						<tr>
							<td>Tanggal Booking</td>
							<td><?php echo $pesanan['tanggal_pesan'];?></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-3">
			</div>
		</div>
		<h5>berikut adalah rincian pesanan kamu</h5>
		<div class="row">
			<div class="col-xs-12">
				<table class="table table-bordered table-striped table-responsive">
					<thead>
						<tr>
							<th style="text-align:center">Perawatan</th>
							<th style="text-align:center">Harga</th>
							<th style="text-align:center">Status</th>
						</tr>
					</thead>
					<tbody>
					<?php 
						if (is_array($detail_pesanan) || is_object($detail_pesanan)){
						foreach($detail_pesanan as $detail_pesanan)
						{
					?>
						<tr>
							<td><?php echo $detail_pesanan['nama_perawatan'];?></td>
							<td style="text-align:right;"><?php echo "Rp. ".$detail_pesanan['harga'];?></td>
							<td style="width:200px;"><?php echo $detail_pesanan['status'];?></td>
						</tr>
					<?php 
						}
						}
					?>
					</tbody>
					<tfoot>
						<tr>
							<th style="text-align:right;font-weight:bold;">
								Total Belum Disetujui </th>
							<th style="text-align:right;font-weight:bold;width :120px;">
								<?php echo $biaya['total_bayar_belum_disetujui'] ? "Rp. ".$biaya['total_bayar_belum_disetujui'].",00" : "-" ;?>
							</th>
						</tr>
						<tr>
							<th style="text-align:right;font-weight:bold;">Total Ditolak</th>
							<th style="text-align:right;font-weight:bold;width :190px;">
								<?php echo $biaya['total_bayar_ditolak'] ? "Rp. ".$biaya['total_bayar_ditolak'].",00" : "-" ;?>
							</th>
						</tr>
						<tr>
							<th style="text-align:right;font-weight:bold;">
								Total Disetujui</th>
							<th style="text-align:right;font-weight:bold;width :190px;">
								<?php echo $biaya['total_bayar_disetujui'] ? "Rp. ".$biaya['total_bayar_disetujui'].",00" : "-" ;?>
							</th>
						</tr>
						<tr>
							<th style="text-align:right;font-weight:bold;">Total Harus Dibayar</th>
							<th style="text-align:right;font-weight:bold;width :190px;">
								<?php echo $biaya['total_bayar_disetujui'] ? "Rp. ".$biaya['total_bayar_disetujui'].",00" : "-" ;?>
							</th>
						</tr>
					</tfoot>
				</table>
			</div>
			
		</div>
		<div class="row">
			<div class="col-xs-12" style="text-align:right;">
				<a href="<?php echo URL_USER."website/booking/kembali.php"?>" class="btn btn-success">Kembali</a>
			</div>
		</div>
	</div>
</div>