<!-- features -->
<div class="feature">
	<div class="container">
		<h3 class="tittle-one">Booking Perawatan</h3>
		<div class="feature-grids">
			<!--<div class="col-md-3 feature-grid text-center">
			</div>-->
			<div class="col-md-6 feature-grid text-center">
				<div class="feature-grid-two">
					<a href="#" class="hi-icon hi-icon-archive fa fa-ticket" data-toggle="modal" data-target="#cek-booking"> </a>
					<h4>Cek Kode Booking</h4>
					<p> Sudah Booking, cek pesanan anda apakah bisa dilayani di sini</p>
				</div>
			</div>
			<div class="col-md-6 feature-grid text-center">
				<div class="feature-grid-one">
					<a href="#" class="hi-icon hi-icon-archive fa fa-bath" data-toggle="modal" data-target="#new-booking"> </a>
					<h4>Booking Perawatan</h4>
					<p> Pesan Perawatan sesuai waktu yang kamu inginkan di sini</p>
				</div>
			</div>
			<!--<div class="col-md-3 feature-grid text-center">
			</div>-->
			<div class="clearfix"></div>
        </div>        
	</div>
</div>

<div class="modal fade" id="cek-booking">
        <div class="modal-dialog">
        <div class="modal-content">
             <form action="<?php echo URL_USER."website/booking/cek_kode_booking.php" ?>" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Cek Kode Booking</h4>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                        <label for="kd_pesanan" class="col-md-3 control-label">Kode Booking</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" id="kd_pesanan" name="kd_pesanan">
                        </div>
                    </div>
                    <br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Cek</button>
                </div>
            </form> 
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="new-booking">
        <div class="modal-dialog">
        <div class="modal-content">
             <form action="<?php echo URL_USER."website/booking/cek_nomor_pelanggan.php" ?>" method="post">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Booking Perawatan</h4>
                </div>
                <div class="modal-body form-horizontal">
                   <div class="form-group">
                        <label for="nama" class="col-md-4 control-label">Nomor Pelanggan</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="nomor_pelanggan" name="nomor_pelanggan" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="tanggal" class="col-md-4 control-label">Tanggal Pemesanan</label>
                        <div class="col-md-8">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker" name="tanggal_pesanan" required>
                            </div>
                        </div>
                    </div>
                    <br><br>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Cek</button>
                </div>
            </form> 
        </div>
        <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>