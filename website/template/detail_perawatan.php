<!-- about -->
	<!--start-about-->
	<div class="about">
		<div class="container">
			<div class="about-bottom l-grids text-center">
				<figure class="effect-bubba">
					<img src="<?php echo $perawatan['foto'] ? URL_USER."upload/".$perawatan['foto'] : URL_USER."images/ban2.jpg" ?>" alt=""/>
					<figcaption>
						<h4><?php echo $perawatan['nama_perawatan'] ?></h4>
                        <h5><?php echo "Rp. ".$perawatan['harga'] ?></h5>
						<p>Kecentikan tidak harus dari wajah tetapi juga dari hati.</p>																
					</figcaption>			
				</figure>
			</div>
            <div class="read-more">
                <h3 class="tittle-one">
                    <a class="hvr-shutter-in-horizontal button" href="<?php echo URL_USER."website/perawatan"?>">Kembali</a>
                </h3>
            </div>
		</div>
	</div>	
<!--//about-->